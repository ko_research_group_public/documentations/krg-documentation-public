Ko Research Group Documentation [MkDocs] website using GitLab Pages.

--

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. [Install][] MkDocs
3. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
4. Add content
5. Generate the website: `mkdocs build` (optional)


> Forked from https://gitlab.com/morph027/mkdocs<br>
> Many features inspired by https://gitlab.com/NERSC/nersc.gitlab.io

[mkdocs]: http://www.mkdocs.org
[install]: http://www.mkdocs.org/#installation
