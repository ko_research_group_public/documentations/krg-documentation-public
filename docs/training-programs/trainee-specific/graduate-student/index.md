# Graduate-Student Training Program

!!! info "Familiarize [Graduate Student Handbook][Handbook]"

## Important Events

??? abstract "Graduate Orientation"
    - typically held one week before 1st semester (need registration)
    - collect student ID card from [Eagle Students Services Center](https://facilities.unt.edu/eagle-student-services-center)
    - [UNT Graduate Orientation Page](https://studentaffairs.unt.edu/orientation-and-transition-programs/programs/orientation/graduate-orientation)
    - proficiency exams
    ??? tip "Preparing for the Chemistry Proficiency Exams"
        Use free GRE Chemistry Test Practice Book available online for self-assessment; please talk to [Hsin-Yu](mailto:hsin-yu.ko@unt.edu) if you need help

    - course selection

    ??? note "Initial Course Registration"
        Due to the proficiency exams, course registration for the first semester in 1YR will happen after these exams (with the help of the graduate administrator).
        
        You will likely be enrolled in (e.g., for Spring 2024):
        
        - 2 core/proficiency courses (6 credits)
        - seminar (1 credit)
        - research hours (2 credits)


## Graduation Requirements

??? success "Course-Releted Graduation Requirements (to be checked)"
    | line items                                                | timeline                            | note                                                    |
    |-----------------------------------------------------------|-------------------------------------|---------------------------------------------------------|
    | pass P.Chem. and 2 other proficiency exams(/courses)      | orientation week(/1YR)              | recommend: preparing and passing the exams              |
    | complete P.Chem. and 1 other core courses                 | 1YR or 2YR                          | recommend: early completion                             |
    | 18 credits of formal lectures (6 from core + 12 advanced) | 1YR or 2YR                          | recommend: transfer of previous M.Sc. credits (up to 6) |
    | CHEM 5010                                                 | Fall of 1YR                         |                                                         |
    | CHEM 5940                                                 | each semester                       |                                                         |
    | file a Degree Plan                                        | before 2YR                          | TBA                                                     |
    | CHEM 6010 sequence                                        | no later than first semester in 3YR | read [Handbook][Handbook] V.B.1 carefully               |
    | CHEM 6950 (2 consecutive semesters)                       | after completing CHEM 6010          | [Handbook][Handbook] V.B.3                              |


    ??? info "Important notes about courses (see [Handbook][Handbook] IV.G.2)"
        - maintain a full course load (at least *9 credits hours*), including research (CHEM 6940) and seminar (CHEM 5940)
        - maintain an average grade of "B" or better
        - no more than one semester of "F" grade on pass/fail courses throughout the program

??? success "Research-Releted  Graduation Requirements (to be checked)"
    | line items                   | timeline                    | note                                                                |
    |------------------------------|-----------------------------|---------------------------------------------------------------------|
    | Select a Research Advisor    | Apr_Fall or Jun_Spring      | [Handbook][Handbook] IV.C; Graduate Research Advisor Selection Form |
    | Select an Advisory Committee | 1YR                         | [Handbook][Handbook] IV.C; Committee Selection Form                 |
    | Select an external examiner  | Before Dissertation Defense | [Handbook][Handbook] IV.C                                           |
    | Progress review with GSSO    | Yearly (PRG)                | [Handbook][Handbook] IV.D                                           |
    | Advisory Committee Meeting   | Yearly (PRG)                | (Optional) [Handbook][Handbook] IV.D                                |
    | Third-Year Research Seminar  | 3YR                         | [Handbook][Handbook] V.B.2                                          |
    | Dissertation Defense         | End of Program              | [Handbook][Handbook] IV.F; plan one semester ahead                  |

    ??? tip "Dissertation Template"
        See [Thesis Manual](https://tgs.unt.edu/theses-and-dissertations/thesis-manual); recommendation: request up-to-date LaTeX template.

!!! tip "Focus on Research"
    While it is important to learn the basics, the success of a Ph.D. student is measured by research activities. 
    As long as the program requirements can be fulfilled, one should prioritize research time.

!!! tip "Plan ahead"
    Review [UNT Academic Calendars by Semester](https://registrar.unt.edu/registration-guides-by-semester.html) each semester


!!! info "[Scholarship Applications](../../optional/career-development/scholarships/index.md)"


## Training Plans

???+ abstract "A Generic Graduate Student Plan"
    ??? example "Orientation (1st Month)"
        - Complete [Standard Training](../../standard-training/index.md)
        - Discuss mutual goals/expectations
        - Determine an initial research project (see below)
        - (Optional) assign additional mentors
        - Add a short bio to the documentation system
        - Review progress and questions at the end

    ??? example "Initial Research Project (1st Year)"
        !!! info "Topic"
            An application using existing development in the research group

        !!! info "Training focus"
            Develop research experience under a closely mentored setting (student could shadow a mentor if necessary)

        !!! info "Manuscript Preparation"
            - carry out data collection and prepare comprehensive scripts to reproduce the study (including check points that does not require massive resources)
            - regularly check in [research notes](../../../operating-procedures/research-notes.md)
            - present the results in slides and discuss a simple logical story to publish
            - write a rough first draft and shadow the mentor (PI or a senior) while the mentor refine the manuscript
            - the converged manuscript will be submitted to a preprint server (and a peer-reviewed journal for revision)
            - the student (with first-author contribution) will attend a conference to present the work and build professional connections (potential collaborators and reference letter writers)

        !!! info "Publication Process"
            - shadow the PI through the manuscript submission and peer-review process
            - (optional) the student and the PI will follow up with the connection built at conferences to reinforce the relationship
            - (optional) the student will record a short video presentation to describe the main results for general audience

    ??? example "Core Research Projects (2nd/3rd/4th Years)"
        !!! info "Topic"
            The student will plan the core research direction with the PI for his/her dissertation (determine an outline and a first set of scientific questions for the thesis)

        !!! info "Training focus"
            use developed research experience to build up research independence 

        !!! info "Manuscript Preparation"
            - carry out the research with more freedom and only mentored during regular or on-demand research meetings
            - the PI will focus on helping the student avoid divergence and serve as a critical reviewer (devil's advocate)
            - the student will attempt to write a good first draft (avoiding scientific criticism as much as possible) and shadow the PI during refinement
            - converged manuscript will be submitted by the student to a preprint server and a peer-reviewed journal for revision
            - planning conference presentation and building connections

        !!! info "Publication Process"
            - same as first project
            - (optional) the student will use the publication to apply and compete for an award
            - (optional) mentor other students: the core project can serve as an alternative to the Initial Research Project of another trainee with the student being the mentor

    ??? example "Wrapping up (4th/5th/6th Years)"
        - Plan for the next step (e.g., postdoc application, national lab, and industry transition)
        - Program requirements (e.g., Dissertation)
        - Making connections and online presence
        - Archive/transition research project


*[1YR]: First Year
*[2YR]: Second Year
*[3YR]: Third Year
*[PRG]: Program Length
*[CHEM 5010]: "Introduction to Graduate Teaching and Research",</br> which includes classes on effective teaching,</br> library resources, scientific presentations,</br> scientific writing, data analysis, scientific ethics,</br> and other topics, as well as faculty research presentations.
*[CHEM 5940]: "Seminar" (1 credit)
*[CHEM 6010]: "Qualifying Examination and Admission to Ph.D. Candidacy" (3 credits)
*[CHEM 6940]: "Individual Research" (1–12 credits)
*[CHEM 6950]: "Doctoral Dissertation credits"
*[TBA]: To be added
*[Apr_Fall]: By Apr. 1 for students starting in the Fall term (earliest date: Oct. 1).
*[Jun_Spring]: By Jun. 1 for students starting in the Fall term (earliest date: Mar. 1).
[Handbook]: https://chemistry.unt.edu/graduate-program/current-students/graduate-policy-bulletin "Graduate Policies"
*[Advisory Committee]: 4 (or more) faculty members:<br/> Hsin-Yu + [2-3] professors from Chemistry Department + [0-1] external member;</br> Divisions: 2 from the P.Chem. and 2 out-of-field.
*[GSSO]: Graduate Student Services Office
