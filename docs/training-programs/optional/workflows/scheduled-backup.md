# Scheduled Backup

!!! info "Context"
    On HPC systems, there are typically separated scratch filesystem (fast but volatile, say `$scr`) and archive storage (slow but backed up, say `$backup`).

!!! tip "Hint"
    - `rsync`
    - `crontab`

??? example "Write a bash script to automatically backup `$scr/project_a/` to `$backup/project_a/`"
    1. Prepare `rsync` script
       ``` bash title="sync.sh (need to be made executable)"
       #!/bin/bash
       rsync -aq $scr/project_a/ $backup/project_a/  # Note: the tailing slashes matter
       ```

    2. Set up `cron` by running `crontab -e` and adjust the backup time (use absolute path to the `sync.sh` script)
       ```
       0 3 * * * /path/to/sync.sh
       ```
