# Optional Training

!!! todo "wishlist"
    - screen (or tmux)
    - gnuplot
    - globus
    - hotpot.ai
    - latex (overleaf)

- [Programming](programming/index.md)
- [Software Packages](software-packages/index.md)
- [Workflows](workflows/index.md)
- [Career Development](career-development/index.md)
