# VMD - Visual Molecular Dynamics

## Feature Highlights

- Multiple trajectories
- Multiple representations
- Selection
- Saved Visualization state
- Rendering


## Further Reading

!!! note "Tutorials"
    - [Official Tutorial](http://www.ks.uiuc.edu/Training/Tutorials/)
    - [Original Paper (Humphrey, Dalke, and Schulten, J. Mol. Graph. 14, 33 (1996).)](https://www.zotero.org/groups/5344838/krg/collections/SBAWBHHP/items/XI7WBS9X/collection)
