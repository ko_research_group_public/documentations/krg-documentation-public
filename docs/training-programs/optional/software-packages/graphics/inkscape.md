# Inkscape

## Feature Highlights

- [import images (link)](https://inkscape-manuals.readthedocs.io/en/latest/import-pictures.html?highlight=link): allow one to organize script generated plots in a WYSIWYG (what you see is what you get) manar and automatically updates when linked images change
- [align](https://inkscape-manuals.readthedocs.io/en/latest/align-and-distribute.html?highlight=align): keeping things aligned
- [clip](https://inkscape-manuals.readthedocs.io/en/latest/clipping-and-masking.html?highlight=clip#clipping-and-masking): select/show only a region of figure under a given mask

!!! tip "getting an idea of inkscape capabilities"
    Browse inkscape [manual content page](https://readthedocs.org/projects/inkscape-manuals/downloads/pdf/latest) ([source cache](https://gitlab.com/hsin-yu.ko/manuals/-/tree/master/Inkscape-Beginners-Guide/source?ref_type=heads))

!!! note "Further Reading"
    - [Official Documentation](https://inkscape-manuals.readthedocs.io/en/latest/)
    - [Books](https://inkscape.org/learn/books/)
