# Pie-Chart Template

## Markdown (`mermaid` admonition)

Syntax:
```
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```

```mermaid
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```

!!! tip "More Details"
    See [this link](http://mermaid.js.org/syntax/pie.html)
