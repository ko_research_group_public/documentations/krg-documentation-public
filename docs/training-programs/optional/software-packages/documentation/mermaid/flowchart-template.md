# Flowchart Template

## Markdown (`mermaid` admonition)

Top-down flowchart:
```
flowchart TD
  B{Segfault?} -->|Yes| C1[Compiler Flag Strategy];
  C1 --> C2{Problem Solved?};
  C2 -->|Yes| E[No Problem];
  C2 -->|No| C3[More Advanced Strategies];
  C3 --> C2;
  B ---->|No| E;
```

!!! tip "syntax"
    - double white spaces are considered syntax errors
    - `{}` is a control flow
    - `||` is a conditional branch
    - `[]` is the printed name of a node

Output:

``` mermaid
flowchart TD
  B{Segfault?} -->|Yes| C1[Compiler Flag Strategy];
  C1 --> C2{Problem Solved?};
  C2 -->|Yes| E[No Problem];
  C2 -->|No| C3[More Advanced Strategies];
  C3 --> C2;
  B ---->|No| E;
```
