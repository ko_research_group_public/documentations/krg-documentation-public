# Mermaid Admonition

!!! info "Diagramming and charting tool"

    JavaScript based diagramming and charting tool that renders Markdown-inspired text definitions to create and modify diagrams dynamically.
[Official Tutorial](https://mermaid.js.org/config/Tutorials.html)


## Tempaltes
- [flowchart](flowchart-template.md)
- [piechart](piechart-template.md)
