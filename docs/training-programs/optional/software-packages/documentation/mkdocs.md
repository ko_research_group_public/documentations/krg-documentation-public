# MkDocs

???+ example "pdf embedding"
    - package: [`mkdocs-pdf`](https://pypi.org/project/mkdocs-pdf/)
    - Usage:
    ```
    ![Alt text](<path to pdf>){ type=application/pdf }
    ```
    or 
    Optionally, you can specify style constraints, e.g.
    ```
    ![Alt text](<path to pdf>){ type=application/pdf style="min-height:25vh;width:100%" }
    ```
