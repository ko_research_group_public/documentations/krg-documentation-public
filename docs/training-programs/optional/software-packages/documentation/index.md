# Optional Training: Documentation Software

- [document processing (pdf)](document-processing/pdf.md)
- [mermaid markdown graphics](mermaid/index.md)
- [typesetting](typesetting/index.md)
- [mkdocs](mkdocs.md)
