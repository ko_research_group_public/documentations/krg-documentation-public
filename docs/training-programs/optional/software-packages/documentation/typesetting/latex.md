# LaTeX

??? tip "Adding Water Mark"
    ???+ example "Snippet to be inserted before `\begin{document}` (modify `My Water Mark`)"
        ```
        \usepackage[printwatermark]{xwatermark}
        \usepackage{tikz}
        \newsavebox\mybox\savebox\mybox{\tikz[color=gray,opacity=0.50]
          \node{My Water Mark};}
        \newwatermark*[allpages,angle=45,scale=4.5,xpos=-10,ypos=0]{\usebox\mybox}
        ```
        ???+ tip "`Extra \endgroup.` error"
            - add the following line before the `documentclass` definition (ref: [this thread](https://tex.stackexchange.com/questions/566088/xwatermark-throws-error))
            ```
            \RequirePackage[2020-02-02]{latexrelease}
            ```
