# MathJax

!!! note "useful resources"
    - [a stackexchange post](https://quantumcomputing.meta.stackexchange.com/questions/49/tutorial-how-to-use-tex-mathjax-to-render-math-notation/76#76)
    - [adding chemistry environment](https://docs.mathjax.org/en/latest/input/tex/extensions/mhchem.html)
    !!! warning "mhchem depends on MathJax v3"
    - [materials for mkdocs MathJax settings](https://squidfunk.github.io/mkdocs-material/reference/math/#mathjax-mkdocsyml)


