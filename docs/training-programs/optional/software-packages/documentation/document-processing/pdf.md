# PDF File Processing Tools

## Ghostscript (`gs`)
!!! info "Free and Cross Platform Software Suite"
    [`gs`](https://www.ghostscript.com/) is an interpreter for the PostScript® and PDF files

    - Use `-sDEVICE=pdfwrite` to work with PDF files


!!! example "Useful Examples"
    - extracting pages (e.g., in this case the first five pages) from a source file `source-file.pdf` to output `pages-1-5.pdf`
      ``` bash
      gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dFirstPage=1 -dLastPage=5 -sOutputFile=pages-1-5.pdf source-file.pdf
      ```
    
    - combining multiple PDF files (e.g., `part-1.pdf`, `part-2.pdf`, and `part-3.pdf`) to form a `combine.pdf`
      ``` bash
      gs -sDEVICE=pdfwrite  -dNOPAUSE -dBATCH -sOutputFile=combine.pdf part-[1-3].pdf
      ```
      !!! tip "Regular Expression (regex) Match"
          The expression `part-[1-3].pdf` in the above example corresponds to a regular expression that identifies files with name of `part-1.pdf`, `part-2.pdf`, and `part-3.pdf`.

!!! info "Frequently Used Arguments"

    | arguments             | effect                  |
    |-----------------------|-------------------------|
    | `-dNOPAUSE`           | no pause after page     |
    | `-g<width>x<height>`  | page size in pixels     |
    | `-sDEVICE=<devname>`  | select device           |
    | `-q`                  | "quiet", fewer messages |
    | `-r<res>`             | pixels/inch resolution  |
    | `-dBATCH`             | exit after last file    |
    | `-sOutputFile=<file>` | select output file      |
