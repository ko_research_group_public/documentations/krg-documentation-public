# GNU/Linux Operation Systems

??? info "GNU/Linux is a family of open-source Unix-like operating systems"
    It provides easy access to free/open-source software packages and a convenient development environment.

    While using GNU/Linux as the primary work environment is optional, we consider it highly productive and recommend it to intereted trainees.

    !!! tip "Recommended Distribution"
        Among the many Linux distributions. A good staring point is [Ubuntu](https://ubuntu.com/).


- [Installation](installation.md)
- [email client settings](outlook365-local-email.md)
