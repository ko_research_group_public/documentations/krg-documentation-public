# Linux Installation

Several methods for installing Linux distributions (Ubuntu for example):

=== "VirtualBox (Risk-Free Assessment)"
    [Oracle VM VirtualBox](https://en.wikipedia.org/wiki/VirtualBox) is a technology for x86 virtualization.
    You can install this software on you current (`host`) operating system (MS Windows or Mac OSX) to create a virtual machine (`guest`) for installing Linux.
    
    - [A Tutorial](https://www.instructables.com/How-to-install-Linux-on-your-Windows/)

    !!! success "Pros"
        - Risk-free: screwing up the `guest` machine will not mess up the host
        - Portable: virtual machine image can be duplicated

    !!! danger "Cons"
        - Overhead: running virtual machine has sizable overhead and may not be as performant as native installation
        - Less scalable: resources (e.g., memory/disk space) allocated by the running `guest` will be unusable by the `host`


=== "Live USB (Low-Risk Assessment)"
    Live USB (or Live CD) provides a device that one can use to boot a computer into a full-fledged Linux distribution.

    - [A Tutorial of Creating Ubuntu Live USB on Windows (also has link inside for Mac OSX)](https://ubuntu.com/tutorials/create-a-usb-stick-on-windows)

    !!! warning "Live USB is also commonly used for Linux installation"
        If your goal is to assess Linux specifically for your machine, you might want to avoid executing the installation shortcut.

    !!! success "Pros"
        - Realistic: live USB provides a good assessment of hardware support as well as operating performance (apart from the initial loading time of each program via USB drive)
        - Low-risk: it does not mess with your Windows/OSX installation if you don't start the Linux installation

    !!! danger "Cons"
        - More technical: one will need to know how to select booting devices [(here is an example)](https://www.instructables.com/Make-a-Live-USB-to-boot-from-a-USB-drive/)

=== "Dual Boot"
    [Install Linux side-by-side with Windows](https://opensource.com/article/18/5/dual-boot-linux)

    !!! success "Pros"
        - Realistic: when you boot into the Linux or Windows, you get full performance for both
        - Keeping Windows option

    !!! danger "Cons"
        - Intermediate-risk: mistakes can mess up your existing data
        - Windows partition takes up disk space (especially, many people stop using Windows after experiencing Linux)
        - Technical

=== "Full Linux Installation"
    By [continuing the Linux installation](https://ubuntu.com/tutorials/try-ubuntu-before-you-install#5-enjoy-ubuntu) using Live USB, one achieves the full installation.

    !!! success "Pros"
        - Realistic: you cannot get more realistic Linux experience than having a full Linux installation
        - Harness full power of the machine

    !!! danger "Cons"
        - Intermediate-risk: mistakes can mess up your existing data
        - Windows partition will be gone
        - Technical (slightly less technical than Dual Boot)
