# Local Email Client with Outlook

## Thunderbird

??? abstract "email and contact"
    Using `ExQuilla` add-on as described in [this tutorial](https://itservices.cas.unt.edu/services/email/articles/add-exchange-email-thunderbird-ubuntu).

??? abstract "calendar"
    Use `TbSync` and `Provider for Exchange ActiveSync` as described in [this tutorial](http://pig.made-it.com/office365_thunderbird.html)
