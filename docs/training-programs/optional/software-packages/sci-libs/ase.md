# Atomic Simulation Environment (ASE)

The [Atomic Simulation Environment](https://wiki.fysik.dtu.dk/ase/) provides a nice interface for processing cube files.

Python workflow library.
!!! todo "prepare a tutorial"
    ref: DOI 10.1088/1361-648X/aa680e

## Visualizing Gaussian Cube Files

``` python
from ase.io.cube import read_cube
cube_file = "foo.cube"
with open(cube_file) as f:
     cube = read_cube(f)
```
