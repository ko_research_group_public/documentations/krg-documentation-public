# Hierarchical Data Format: HDF5

!!! info "[Hierarchical Data Format](https://en.wikipedia.org/wiki/Hierarchical_Data_Format)"
    A set of file formats (HDF4, HDF5) designed to store and organize large amounts of data in a portable fashion.

    Features:
    - portable
    - compressible
    - parallelizable


!!! example "Templates"
    - [`hdf5_templates.py`](https://gitlab.com/ko_research_group/software/code-templates/-/blob/main/python/io/hdf5_templates.py): see example use cases near the end of the file.

!!! note "useful data space visualization"
    - [Browse pictures in "Reading From or Writing To a Subset of a Dataset"](https://docs.hdfgroup.org/hdf5/develop/_l_b_dset_sub_r_w.html)


## Reading List

- [Official Tutorial](https://docs.hdfgroup.org/hdf5/develop/_learn_basics.html)
- [Chapter 7 Scientific Data Storage, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf)
