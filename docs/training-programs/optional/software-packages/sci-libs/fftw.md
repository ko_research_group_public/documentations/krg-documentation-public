# FFTW3
!!! note "user manual"
    [Official Tutorial](https://www.fftw.org/fftw3_doc/index.html)

!!! note "meeting with Ju-an Zhang (2024-01-04)"
    - [column-major convention note](https://www.fftw.org/fftw3_doc/Reversing-array-dimensions.html)
    - normalization convention
    \[
    \mathrm{invFFT}(\mathrm{fwdFFT}(f(x))) = Nx * f(x)
    \]
    - using `FFTW_MEASURE` or `FFTW_ESTIMATE` in fft plan ([referred doc](https://www.fftw.org/fftw3_doc/Complex-One_002dDimensional-DFTs.html)): in short, if need to use the same setting many times use `FFTW_MEASURE` (getting optimzed performance parameters by measuring the current system); otherwise, use `FFTW_ESTIMATE` to get a default set of parameters (sidestep the overhead of measurement).

!!! todo "examples"
    add reference to Ju-an Zhang's Hockney toy code when available.
