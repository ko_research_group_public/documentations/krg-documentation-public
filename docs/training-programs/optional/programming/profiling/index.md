# Profiling

!!! abstract "Textbook Reading"
    - [Chapter 16 Profiling and benchmarking, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf)
