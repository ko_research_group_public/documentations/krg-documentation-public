# Debugging

!!! abstract "Textbook Reading"
    - [Chapter 11 Debugging, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf)

- [Dealing with Segmentation Faults](segmentation-fault.md)
- [GDB](gdb.md)
