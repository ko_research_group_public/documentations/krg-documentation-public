# Segmentation Fault (Segfault)

!!! info "Trying to access a memory location without permission"
    e.g., when going out of bound of an array

## Workflow

``` mermaid
flowchart TD
  B{Segfault?} -->|Yes| C1[Compiler Flag Strategy];
  C1 --> C2{Problem Solved?};
  C2 -->|Yes| E[No Problem];
  C2 -->|No| C3[More Advanced Strategies];
  C3 --> C2;
  B ---->|No| E;
```

## Compiler Flag Strategy

Fortran provides useful compiler flags for debugging:

| Function                                 | GNU           | Intel        | NVHPC (PGI) |
|------------------------------------------|---------------|--------------|-------------|
| source annotation (give line numbers)    | `-g`          | `-g`         | `-g`        |
| provide stack trace when Segfault        | `-fbacktrace` | `-traceback` |             |
| extensive checks (array bounds..., slow) | `-fcheck=all` | `-check all` | `-C`        |

## More Advanced Strategies

### Use a Debugger

- See [`gdb` Tutorial](gdb.md)

### Use a Simple Reproducer

Favorable simplifications:

- serial
- quick to reproduce (within seconds)
- deterministic (no random reproducibility)
- (optional) easy to compile

### Bisection of the Codebase

!!! todo "prepare more info"
