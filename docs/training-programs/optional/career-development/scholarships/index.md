# Scholarships

!!! info inline end "Definition in [Merriam-Webster](https://www.merriam-webster.com/dictionary/scholarship)"
    money given (as by a college) to a student to help pay for further education.

??? info "Find specific eligible scholarships"
     is a collection of scholarships

    - read scope, objectives, and limitations of each scholarship
    - use their language in your essay

??? tip "Some tips for writing scholarship essay"
    Emphasizing what sets you apart from other applicants, e.g.:

    ??? abstract "Personal Introduction"
        Start by introducing yourself (name and country of origin) and explicitly mention your enthusiasm (use the explicit name of scholarship).

    ??? abstract "Highlight Unique Strengths"
        For instance, if you have invented something emphasize your inventor aspect and give concrete example.

        If you have good grades, provide GPA (or other appropriate metrics) to highlight your academic performance.

        If you have research experience, showcase your track record (or publications).

        Include your leadership experiences if any.

        These aspects underscore your distinctiveness.

    ??? abstract "Address Unique Challenges"
        Discuss being a `[first-generation interational Ph.D. student]` and any financial constraints. Highlight how the scholarship will help you overcome these challenges, differentiating you from fellow applicants and enabling you to thrive at UNT.

    ??? abstract "Future Contributions to UNT"
        Express your intention to leverage your experiences to become a UNT alumni ambassador and an active member of the UNT alumni association post-graduation. This shows your long-term commitment and potential impact.


??? tip "additional tips"
    It is probably a good idea to do the following when preparing the application essay:
    
    1. Identify scholarships that you are eligible for and sort them in descending interest order
    2. Extract the common criteria and objectives of these scholarships (and note the specific differences) of your top three scholarships
    3. Do a dry run on the application website (fill in the form with placeholder information, similar to what we did to your COS form) so that you know the exact materials to prepare (e.g., if we can use a custom PDF file for your scholarship essay or if they want the essay to be pasted directly to an online form)
    4. Send an email to the contact of each of your top three scholarships to request any format requirements or templates (if there is no explicit requirements outlined).



## UNT Scholarships

- [UNT Alumni Association Scholarship Program](https://untalumni.com/students/scholarships/)
- [UNT College of Science Scholarships](https://cos.unt.edu/scholarships)

