# Getting Started (Lab Orientation)

???+ success "Welcome to the Ko Research Group"
    We are excited to have you as a new member.
    Our group is dedicated to pushing the boundary of first-principles
    discovery/prediction of complex condensed-phase chemical reactions.

    In addition to our scientific focus, we are committed to training our members to
    maximize their career success. For this purpose, we have prepared the following
    onboarding training.

!!! info inline end "Training Programs"
    ```mermaid
       flowchart TD
         A[Standard Training] --> B;
         B{Trainee Identity?} -->|Add Specific Modules| D;
         D[Initial Plan] -->|Trainee Objectives| E[Active Training Plan];
         E --> F[Regular Reviews];
         F -->|Refine| E;
    ```

## Common Modules

!!! abstract "[Standard Training](standard-training/index.md)"

!!! abstract "[Optional Training](optional/index.md)"

## Trainee-Specific Modules

!!! abstract "[Graduate Students](trainee-specific/graduate-student/index.md)"

!!! abstract "[International Students](https://docs.ko-research.org/training-programs/trainee-specific/international-student/)"
