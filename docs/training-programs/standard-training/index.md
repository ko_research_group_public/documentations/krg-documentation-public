# Standard Training: Overview

## Expectations

- [Code of Conduct](code-of-conduct.md) (ETE: 20-30 minutes)

??? question "Checklist"
    - [ ] the expectations on you
    - [ ] what to expect from the group

## Relevant [Standard Operating Procedures (SOPs)](../../operating-procedures/index.md)

- [Account Setup](../../operating-procedures/account-setup.md) (ETE: 10-20 minutes)
- [Meetings](../../operating-procedures/meetings.md) (ETE: 10 minutes)
- [Questions](../../operating-procedures/questions.md) (ETE: 10-20 minutes)
- [Time Management](../../operating-procedures/time-management.md) (ETE: 10-20 minutes)


??? question "Checklist"
    - [ ] access to [the KRG Team](https://gitlab.com/ko_research_group/team) group on GitLab
    - [ ] access to [the KRG Teams channel](https://teams.microsoft.com/l/team/19%3a-A-EbmV8yIKIOEzuJ9nPIc3PxEQ-v3QOMm8qFYmzI-w1%40thread.tacv2/conversations?groupId=5835056d-9cc3-4168-b05b-5daec2330794&tenantId=70de1992-07c6-480f-a318-a1afcba03983) via the UNT account
    - [ ] access to [the CRUNTCH cluster](hpc-platforms/cruntch4/index.md) via the UNT account
    - [ ] access to [KRG Zotero Group Library](https://www.zotero.org/groups/5344838/krg/library)
    - [ ] on-demand meeting scheduling
    - [ ] question/issue handling

## Core Skills

- [Software](software/index.md) (ETE: 2-3 weeks)
- [Research](research/index.md) (ETE: TBD)

??? question "Checklist"
    - [ ] access to a Unix-like work enviroment or a working setup of [the KRG base development environment docker container](https://hub.docker.com/u/koresearch/develop-environment-base)
    - [ ] Unix/Linux basics: shell/bash commands
    - [ ] `git` basics: `clone`, `add`, `commit`, `push`, `pull`, `branch`, `checkout`
    - [ ] `vim` basics
    - [ ] running electronic structure calculations on an HPC cluster
    - [ ] write research notes and manage references

*[ETE]: Estimated Trainee Effort
