# Using High-Performance Computing Platforms

???+ info "HPC platform components"
    ??? example "processing units"
        ???+ example "CPU"
            - central processing units
            - information can be accessed by `lscpu`
        ???+ example "GPU (NVidia)"
            - graphical processing units
            - information can be accessed by `nvaccelinfo` and `nvidia-smi`

    ??? example "Memory"
        - monitor with `free`, `vmstat`, or `top
        - more information `cat /proc/meminfo` 

    ??? example "filesystems"
        ???+ example "`$HOME` (`~`)"
            - your home folder typically backed up and limited in storage quota
            - should be used to store source code and setting files

        ???+ example "scratch filesystem (varies with systems, e.g., `/scratch`)"
            - fast filesystem with large quota, where the calculations should be run
            - typically not backed up; as such, relevant files need to be staged to safer places (archive filesystem) once calculations are done
            - read system documentation, ask system administrator, or analyze output of `df`

        ???+ example "archive filesystems"
            - large quota and backed up but relatively slow filesystem, where the organized research data extracted from raw calculations should be stored
            - read system documentation, ask system administrator, or analyze output of `df`

## Relevant HPC Systems

- [CRUNTCH4](cruntch4/index.md)
