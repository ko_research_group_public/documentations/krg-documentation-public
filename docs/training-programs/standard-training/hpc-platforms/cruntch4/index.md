# CRUNTCH4 Cluster at the CASCaM Center in UNT Chemistry

## Remote Login

???+ info "Connecting from Off Campus"
    First sign in to [UNT VPN](https://itservices.cas.unt.edu/services/accounts-servers/articles/cisco-anyconnect-mobility-client-vpn).

    ``` shell
    ssh <euid>@cruntch4.chem.unt.edu
    ```

    ???+ tip "openconnect: cisco-anyconnect alternative"
        Some linux users may encounter issues with cisco anyconnect.
        An alternative (some argue better than anyconnect) is [openconnect](https://www.infradead.org/openconnect/).

        For Ubuntu users ([see this thread](https://askubuntu.com/questions/154699/how-do-i-install-the-cisco-anyconnect-vpn-client)), do the following:
        
        - `sudo apt-get install network-manager-openconnect-gnome`
        - set openconnect protocol to "Cisco AnyConnect"
        - set openconnect gateway to "vpn.unt.edu"
        - start connection by typing UNT credentials (EUID and password)


## System-Specific Settings

???+ example "example submission scripts"
    Samples can be found at `/storage/nas_scr/shared/slurm_scripts/`

???+ example "filesystems"
    - Home: `$HOME`
    - Network Attached Storage (NAS) scratch: `/storage/nas_scr/$USER`
    - Archive storage: `/archive/$USER`

???+ tip "make a link to the scratch partition"
    ```bash
    cd ~ # go home
    ln -s /storage/nas_scr/$USER scr  # make a link named scr at home that points to the NAS scratch where calculations happen
    ```

## Compilation of Quantum ESPRESSO

=== "(CPU) mkl impi"
    ``` shell
    #!/bin/bash
    module load INTEL/compiler-rt/2022.2.1 INTEL/compiler/2022.2.1
    module load INTEL/mkl/2022.2.1
    module load INTEL/mpi/2021.7.1
    module load INTEL/tbb/2021.7.1   
    module load INTEL/oclfpga/2022.2.1
    module load git/2.33.1

    WRK_DIR=build.cruntch4.cpu

    cmake  -DCMAKE_CXX_COMPILER=icpc -DCMAKE_C_COMPILER=icc -DCMAKE_Fortran_COMPILER=mpiifort \
    -DQE_ENABLE_OPENMP=ON -DCMAKE_Fortran_FLAGS='-O3 -g -cpp -qopenmp -qopenmp-simd' -B $WRK_DIR

    cd $WRK_DIR
    make -j pw
    ```


=== "(GPU) mkl impi cuda11.7"
    ``` shell
    #!/bin/bash

    module load INTEL/compiler/2022.2.1
    module load INTEL/mkl/2022.2.1
    module load INTEL/mpi/2021.7.1
    module load cuda11.7/toolkit/11.7.1
    module load nvhpc/22.7
    module load git/2.33.1

    WRK_DIR=build.cruntch4.gpu

    cmake -DQE_ENABLE_OPENMP=ON -DQE_ENABLE_CUDA=ON -DQE_ENABLE_MPI=OFF  -DQE_ENABLE_OPENACC=ON \
    -DQE_ENABLE_PROFILE_NVTX=ON -DCMAKE_Fortran_FLAGS='-O3 -g -fast -acc -gpu=cc80 -Mcache_align -Mpreprocess -Mlarge_arrays -gpu=managed' -B $WRK_DIR

    cd $WRK_DIR
    make -j 32 pw
    ```

=== "(GPU) mkl ompi cuda11.7"
    ``` shell
    #!/bin/bash

    module load INTEL/mkl/2022.2.1
    module load OPENMPI/gcc12.2/cuda11.7.1/4.1.4
    module load cuda11.7/toolkit/11.7.1
    module load nvhpc/22.7
    module load git/2.33.1

    WRK_DIR=build.cruntch4.gpu.ompi

    cmake -DQE_ENABLE_OPENMP=ON -DQE_ENABLE_CUDA=ON -DQE_ENABLE_MPI=OFF  -DQE_ENABLE_OPENACC=ON \
    -DQE_ENABLE_PROFILE_NVTX=ON -DCMAKE_Fortran_FLAGS='-O3 -g -fast -acc -gpu=cc80 -Mcache_align -Mpreprocess -Mlarge_arrays -gpu=managed' -B $WRK_DIR

    cd $WRK_DIR
    make -j 32 pw
    ```
