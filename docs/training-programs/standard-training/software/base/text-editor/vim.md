# Standard Training: VIM - Vi IMproved, a programmer's text editor

??? tip "HPC are often used remotely via command line operations."
    Familiarity with a text-editor without graphical user interface (GUI) is important to your productivity as a computational scientist.

    `vim` (sometimes aliased to `vi`) is also the most commonly available text editor on supercomputers.

??? tip "High Productivity after Steep Learning Curve"
    Training to use a text editor might sound ridiculous, but a editor can make game-changing productivity (with an unexpected learning curve).

    When used for HPC, it might be good to choose those with CLI.

    !!! info "vim has a steep learning curve because it has several editing modes"
        It is worth investing time in overcoming this learning curve as the editing modes enables many techniques for fast editing.

!!! example "tutorials"
    Try build-in tutorial for vim by:
    ```bash
    vimtutor
    ```

!!! note "compare files"
    - `vimdiff file1 file2`
    - `vim -d file1 file2`

??? tip "visual block mode"
    - In normal mode, use `Control-v` to trigger visual block mode.
    - Select a visual block (more than one line), inset text with `Shift+i` (capital `I`) to insert text; press `Esc` to finish inserting same text in all lines of the block.
    - Select a visual block (more than one line), press `d` to delete the block (can be pasted as a block at the cursor by `p` in normal mode)


Lowercase i will not work.

Then type the things you want and finally to apply it to all lines, press Esc twice.

## External Resources

- [Arunachalam B's Tutorial for Beginners](https://www.freecodecamp.org/news/vim-beginners-guide/)
- [Interactive Tutorial](https://www.openvim.com/)
- [More extensive tutorial from linuxconfig.org](https://linuxconfig.org/vim-tutorial)

!!! example "Alternative editors"

    - emacs
    - nano
