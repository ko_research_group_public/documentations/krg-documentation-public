# Standard Training: Software: Base Tools

!!! abstract "[Unix-like Systems](unix-like-systems/index.md)"

!!! abstract "[Vim](text-editor/vim.md)"

!!! abstract "[DevOPs](devops/index.md)"
