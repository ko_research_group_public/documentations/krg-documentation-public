# Standard Training: Unix-Like Systems

!!! abstract "Textbook Reading"
    - [Chapter 1 Unix intro, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf)
    - [(Optional) Tutorial](https://web.corral.tacc.utexas.edu/CompEdu/pdf/stc/hpc_unix.pdf)

???+ question "checklist: proficiency in level 1 bash commands"

    | type            | commands                                   |
    |-----------------|--------------------------------------------|
    | list files      | `ls`                                       |
    | getting text    | `cat`, `head`, `tail`, `less`              |
    | file operations | `cp`, `mv`, `rm`                           |
    | directories     | `cd`, `pwd`, `mkdir`, `rmdir`              |
    | paths           | `.`, `..`, `~`, `/`                        |
    | help            | `man`                                      |
    | redirection     | `|`, `>`, `<`, `stdin`, `stdout`,`stderr`  |

???+ question "checklist: proficiency in level 2 shell/bash commands"

    | type              | commands                  |
    |-------------------|---------------------------|
    | environment       | `echo`, `env`, `export`   |
    | permission        | `chmod`, `chgrp`, `chown` |
    | startup           | `~/.bashrc`               |
    | archive/compress  | `tar`, `gzip`             |
    | text modification | `grep`, `awk`, `sed`      |
    | directory jumping | `pushd`, `popd`, `dirs`   |
    | synchronization   | `rsync`                   |

???+ question "checklist: proficiency in level 3 shell/bash commands"

    | type          | commands                              |
    |---------------|---------------------------------------|
    | processes     | `ps`, `pkill`, `pgrep`, `top`         |
    | jobs          | `jobs`, `Ctrl-c` `Ctrl-z`, `fg`, `bg` |
    | command macro | commands in backquotes                |
    | search paths  | [`$PATH`, `$LD_LIBRARY_PATH`][PATHs]  |
    | scripting     | for-loop, condition, arguments        |

???+ tip "`TAB` Completion of Partial Commands and Paths" 
    When using interactive shell environments (e.g., Bash), one can do command/path completion by pressing `TAB`. More information can be found in [this page](https://www.gnu.org/software/gnuastro/manual/html_node/Bash-TAB-completion-tutorial.html).

## KRG Additions

- [slurm](slurm/index.md)
- [`ssh`](ssh.md)
- [`find` and `locate`](find.md)

???+ tip "`ls` tricks"
    - `ls -a` shows hidden files
    - `ls -lh` shows details of the files in a human readable manner
    - `ls -lrt` shows details of the files in chronological order (newest at the end)
    - `ls -v` use nature sort order (e.g., default order "1 100 2 3" becomes "1 2 3 100"); see also: [this thread](https://unix.stackexchange.com/questions/33909/list-files-sorted-numerically)

???+ tip "`cd` tricks"
    - `cd` or `cd ~` goes to user home directory
    - `cd -` jumps to previous directory
    - `cd ..` goes to parent directory

???+ note "fancier tools (typically need to be compiled before use)"
    - [ripgrep `rg`](https://github.com/BurntSushi/ripgrep)
    - [ripgrep-all `rga`](https://github.com/phiresky/ripgrep-all)

???+ note "Working with MS Windows"
    - [unix2dos](unix2dos.md)

[PATHs]: path.md
