# Tool: GNU Findutils
!!! info "Powerful directory search and file locating capabilities"
    - `find`
    - `locate`
    - `updatedb`
    - `xargs`

## `find`: Searches a Directory Tree
!!! example "file with an extension (e.g., svg)"
    ``` bash
    find . -name \*.svg
    ```
    !!! note "special characters"
        - `\`: provides escaped character (delaying wildcard character `*`)
        - `*`: is a wildcard character, indicating anything can be acceptable

!!! example "file within a range of dates"
    ``` bash
    find -newermt "2022-12-01" ! -newermt "2022-12-31"
    ```

    !!! note "special characters"
        - `!`: denotes "not"

!!! example "combined use"
    ``` bash
    find -name \*.svg  -newermt "2022-12-01" ! -newermt "2022-12-31"
    ```

!!! note "further reading"
    [`fd`: A simple, fast and user-friendly alternative to `find`](https://github.com/sharkdp/fd)

!!! todo "add more content"
