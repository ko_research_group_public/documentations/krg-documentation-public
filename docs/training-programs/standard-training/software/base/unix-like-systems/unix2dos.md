# Software: unix2dos - CRLF Conversion

Convert line breaks from Unix format (Line feed, LF, `\n`) to DOS format (carriage return + Line feed, i.e., CR+LF, `\r\n`) and vice versa, see the [unix2dos](https://en.wikipedia.org/wiki/Unix2dos) and [newline](https://en.wikipedia.org/wiki/Newline) wikipedia entries for longer discussions.

???+ tip "How to use"
    - convert file generated on unix systems (e.g., Linux and MacOSX) to dos (MS Windows)
    ```bash
    unix2dos my-file-from-unix
    ```
    - convert file generated on from dos to unix
    ```bash
    dos2unix my-file-from-dos
    ```
