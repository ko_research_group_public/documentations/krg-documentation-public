# Slurm

## KRG In a Nutshell

??? tip "core slurm commands"
    - `sinfo`: provides slurm information (e.g., partitions and status)
    - `squeue`: prints information about current queue (use `-u $USER` for your own jobs)
    - `sbatch`: submit a batch job
    - `srun`: (used in batch script) slurm wrapper of running a job using allocated resources
    - `scancel`: cancel a job

??? tip "some examples"
    - [KRG Templates](templates.md)

## Machine-Specific Settings

- [CRUNTCH4](cruntch4.md)
- [NERSC](nersc.md)

## Reading List

- [Chapter 18 Slurm, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf)
