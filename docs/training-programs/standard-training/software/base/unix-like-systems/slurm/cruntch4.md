# Slurm: CRUNTCH4 Examples

??? example "A header CRUNTCH4 jobs"
    ```bash
    #!/bin/bash
    #SBATCH -p <partition>
    #SBATCH -t 01:00:00
    #SBATCH -n <number-of-mpi-processes>
    ```
    ??? tip "access available partitions"
        - run `sinfo`
