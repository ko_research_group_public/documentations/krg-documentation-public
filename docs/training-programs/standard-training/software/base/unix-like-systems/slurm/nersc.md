# Slurm: NERSC Examples

- [Running Jobs (NERSC Docs)](https://docs.nersc.gov/jobs/)

??? example "A header for CPU architecture (Perlmutter)"
    ```bash
    #!/bin/bash
    #SBATCH -A m2655
    #SBATCH -C cpu
    #SBATCH -q regular
    #SBATCH -t 04:00:00
    #SBATCH -N 4
    ```
