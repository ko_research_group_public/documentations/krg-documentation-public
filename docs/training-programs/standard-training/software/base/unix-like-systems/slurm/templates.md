# Slurm: Templates

??? example "High-Throughput (Ensemble) Submission"
    ``` bash
    #!/bin/bash
    #<add appropriate SBATCH arguments>

    tgt_out=DONE

    create_input(){
      cat << EOF > input
    &CONTROL
      calculation = "scf",
      restart_mode = "from_scratch",
      pseudo_dir = ".."
      tstress = .true.
      tprnfor = .true.
    /
    &SYSTEM
       ibrav     = 0
       nat       = 288
       ntyp      = 2
       ecutwfc   = 85.0
    /
    &ELECTRONS
    /
    EOF
    cat $tmpl >> input
    }

    run_a_job(){
      mkdir -p $WK_d
      cd $WK_d
      create_input

      export OMP_NUM_THREADS=2
      # 1 PM-CPU node has 128 cores (broken down to 128 MPI * 2 OpenMP-hyperthreads)
      srun -u -n 128 -N 1 -c 2 $PW < input  > output || exit 1

      touch $tgt_out
      cd ..
    }

    for i in supercell-*.in
    do
      tmpl=`realpath $i`
      WK_d=scr.$i
      if [[ ! -f $WK_d/$tgt_out ]]; then
        while [[ `jobs | wc -l` -ge $[SLURM_JOB_NUM_NODES] ]];
        do
          sleep 1
        done
        run_a_job &
      fi
    done

    wait
    ```
