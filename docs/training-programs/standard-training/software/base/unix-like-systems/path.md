# Search Path Environment Variables: `PATH` and `LD_LIBRARY_PATH`

??? abstract "`PATH`"
    `PATH` specifies a set of directories (separated with `:`) where executable programs are located.

    ??? example "specify PATH (replace `<my_new_path>` with absolate path of a folder"
        ```bash
        export PATH=$PATH:<my_new_path>
        ```
        !!! tip "Include in future bash instances"
            include the above `export` line to bash run commands (`~/.bashrc`) so that it is executed at start of each bash instance.

??? abstract "`LD_LIBRARY_PATH`"
    `LD_LIBRARY_PATH` specifies a set of directories (separated with `:`) where dynamic libraries are located.
    !!! todo "add examples"
