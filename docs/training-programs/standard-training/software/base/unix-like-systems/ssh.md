# Standard Training: SSH - Secure Shell Login

!!! abstract "cryptographic network protocol for, e.g., remote command-line login"

## Basic Features

### Remote Login

???+ example "`ssh`: remote login"
    - login to remote `host` using current username (i.e., `$USER` envirnment variable in Unix-like systems):
    ```bash
    ssh host
    ```

    - login to remote `host` using specific username (e.g., `foo`):
    ```bash
    ssh foo@host
    ```

???+ example "`ssh-keygen`+`ssh-copy-id`: password-less login (public-private key pairs)"
    !!! warning "Be careful not to overwrite your existing ssh keys"
        - **First check for existing keys**
        ``` bash
        ls ~/.ssh
        cat ~/.ssh/id_rsa.pub # get unmodified content of the public key (for copying)
        # on another machine paste public key in ~/.ssh/authorized_keys for keyless login
        ```

    - generate a ssh key pair using [RSA cryptosystem](https://en.wikipedia.org/wiki/RSA_(cryptosystem))
    ```bash
    ssh-keygen  # continue pressing the enter key to use default settings
    ls ~/.ssh   # check the new key pairs
    ```

    !!! warning "Security Note"
        - `~/.ssh/id_rsa`: keep it secret (**do not share**)
        - `~/.ssh/id_rsa.pub` share it for remote authentification (see below)

    - install public key to remote server `host` (please replace `host` with where you are copying your public key):
    ``` bash
    ssh-copy-id host     # run this on your local computer (not on the remote host)
    ```

    ??? tip "Underneath the hood (more traditional approach)"
        ``` bash
        cat ~/.ssh/id_rsa.pub # get unmodified content of the public key (for copying)
        # on another machine paste public key in ~/.ssh/authorized_keys for keyless login
        ```

### Remote File Copy

???+ example "`scp`: remote copy over ssh"
    - `scp` works similar to `cp` and copies file `foo` from `user1@host1` to `user2@host2` as `bar`:
    ```bash
    scp user1@host1:/path/to/foo user2@host2:/path/to/bar
    ```
    - When `host` and `user` are not specified they are treated as your `localhost` and `$USER`; if a path is not given, the path is assumed to be in the `$HOME` folder. As an example, to copy a file `output` from `$HOME` on cruntch4 back to current directory of the local machine can be done via:
    ```bash
    scp <euid>@cruntch4.chem.unt.edu:output .
    ```
    - To copy, multiple files in one-shot, one can first make a tarball via `tar` and do `scp`:
    ```bash
    tar -cf folder.tar folder/
    mv folder.tar ~
    scp <euid>@cruntch4.chem.unt.edu:folder.tar .
    ```
    - Other remote copying options are: `sftp` or `sshfs`. One may also use a GUI to help; see [this thread](https://askubuntu.com/questions/94665/what-is-a-program-similar-to-winscp) for some options



## More Advanced Features (Optional)

??? example "`~/.ssh/config`: custom ssh configurations"
    To login to hosts with longer name or with a different user id, one can add custom configurations in `~/.ssh/config`

    - simplify ssh login to from `ssh EUID@cruntch4.chem.unt.edu` to `ssh cruntch4`, one can add the following lines in `~/.ssh/config` (replace `EUID` with your EUID):
    ```
    Host     cruntch4
    User     EUID
    HostName cruntch4.chem.unt.edu
    ```

??? example "enable graphical user interface (slow)"
    ``` bash
    ssh -Y host
    ```

??? example "ssh (VPN) tunnel"

    - build the tunnel with the host and connect to a local port (`5000` in this case)
    ``` bash
    ssh -D 5000 -N host     # 5000 is an arbitrary port
    ```

    - set up browser proxy to redirect traffic, e.g., when using Firefox change [Connection settings](https://support.mozilla.org/en-US/kb/connection-settings-firefox) to:
    ![img](images/ssh-proxy-setting.png)

## FAQ about SSH

??? question "Can I use the same SSH for multiple servers?"
    Yes, it is generally safe and convenient to do so.
    Related discussions can be found in [this thread](https://unix.stackexchange.com/questions/698060/ssh-public-key-on-multiple-servers) and [this thread](https://serverfault.com/questions/170682/using-same-ssh-private-key-across-multiple-machines)

## Reading List

- [1.14 Connecting to other machines: ssh and scp, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf)
