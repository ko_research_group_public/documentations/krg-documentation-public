# Software Containers

!!! tip "Why Using a Software Container?"
    A simple and lightweight solution to reproducible science; see "An introduction to Docker for reproducible research" ([paper](https://dl.acm.org/doi/10.1145/2723872.2723882)/[pre-print](https://arxiv.org/abs/1410.0846))

    ??? info "KRG Use Cases"
        - Tutorials
        - GitLab CI/CD
        - Distribution of pre-release software
        - Reproducers of published works

## Docker

### Level 1: Using Images

??? tip "You will need to add `sudo` prior to `docker` when using Ubuntu"
    Since docker runs as `root` (super user), you may need to run `sudo docker` instead of `docker` on some operating systems.

- [install docker engine](https://docs.docker.com/engine/install/)
- run container by (`docker run -it <img>`) using a given docker image (`<img>`, which can be replaced by, e.g., one of the containers of our group):

=== "with `sudo` (Ubuntu)"
    ```bash
    sudo docker run -it koresearch/qe
    ```
=== "without `sudo`"
    ```bash
    docker run -it koresearch/qe
    ```
- one can also share files between the host (your operating system) and the guest (docker image) by mounting the host volume to the guest (see [this thread](https://stackoverflow.com/questions/30183978/bind-a-directory-to-a-docker-container) for more information):

=== "with `sudo` (Ubuntu)"
    ```bash
    sudo docker run -v /host/project_folder:/container/project -it koresearch/qe
    ```
=== "without `sudo`"
    ```bash
    docker run -v /host/project_folder:/container/project -it koresearch/qe
    ```

!!! example "Project: Getting KRG Program Environment"
    - [koresearch/develop-environment-base](https://hub.docker.com/r/koresearch/develop-environment-base)


### Level 2: Building and Managing Images

[Build a Docker Image to Dockerhub](https://www.techrepublic.com/article/how-to-build-a-docker-image-and-upload-it-to-docker-hub/):

!!! example "login (one-time) to host and build an image"
    === "with `sudo` (Ubuntu)"
        ```bash
        sudo docker login [registry_host] # when [registry_host] is absent it is assumed to be Dockerhub; you will be prompt for username and password(/token) 
        sudo docker build -t [registry_host/]<container-tag> . # build from "Dockerfile" in the current directory
        sudo docker push [registry_host/]<container-tag>
        ```
    === "without `sudo`"
        ```bash
        docker login [registry_host] # when [registry_host] is absent it is assumed to be Dockerhub; you will be prompt for username and password(/token) 
        docker build -t [registry_host/]<container-tag> . # build from "Dockerfile" in the current directory
        docker push [registry_host/]<container-tag>
        ```

    ??? example "an example"
        - [KRG-Dev-Lite](https://gitlab.com/ko_research_group/software/dockerfiles/-/tree/main/develop-environment/cpu.lite?ref_type=heads)

!!! example "managing images"
    === "with `sudo` (Ubuntu)"
        - get images from remote `registry_host`:
        ```bash
        sudo docker pull [registry_host/]<container-tag>
        ```
        - show existing images
        ```bash
        sudo docker images
        ```
        - remove an image
        ```bash
        sudo docker rmi <image-id>
        ```
    === "without `sudo`"
        - get images from remote `registry_host`:
        ```bash
        docker pull [registry_host/]<container-tag>
        ```
        - show existing images
        ```bash
        docker images
        ```
        - remove an image
        ```bash
        docker rmi <image-id>
        ```

!!! info "Other containers exist to address problems of docker"
    While docker has [limitations](https://medium.com/@jesus.cantu217/what-is-docker-uses-limitations-a10eb5ccec24), it provides a good starting point for building containers.
    Docker containers that are compliant to [the Open Container Initiative](https://opencontainers.org/) can be used with other container technologies (e.g., `singularity`) to sidestep limitations.

??? tip "Additional Tutorials"
    - [Introduction to Docker (by Matthew Feickert)](https://matthewfeickert.github.io/intro-to-docker/)
    - [Docker Tutorial for Beginners (video)](https://youtu.be/pTFZFxd4hOI?si=7EDH6pT-1ubxXepr)
    - [How to Build Docker Images](https://devopscube.com/build-docker-image/)

    !!! example "work with local file system"
        - [File I/O with Containers](https://matthewfeickert.github.io/intro-to-docker/04-file-io/index.html)

### Level 3: More Advance Topics

!!! example "Using (Private) GitLab Container Registry"
    - [setup a token on GitLab](https://gitlab.com/-/user_settings/personal_access_tokens) for `read_registry` and `write_registry`
    === "with `sudo` (Ubuntu)"
        ```bash
        sudo docker login registry.gitlab.com
        sudo docker build -t registry.gitlab.com/<usr-id>/<proj-id> .
        sudo docker push registry.gitlab.com/<usr-id>/<proj-id>
        ```    
    === "without `sudo`"
        ```bash
        docker login registry.gitlab.com
        docker build -t registry.gitlab.com/<usr-id>/<proj-id> .
        docker push registry.gitlab.com/<usr-id>/<proj-id>
        ```

!!! example "process handling"
    === "with `sudo` (Ubuntu)"
        - show existing docker processes
        ```bash
        sudo docker ps
        ```
        - attach to existing docker processes
        ```bash
        sudo docker attach <process-id>
        ```
    === "without `sudo`"
        - show existing docker processes
        ```bash
        docker ps
        ```
        - attach to existing docker processes
        ```bash
        docker attach <process-id>
        ```

!!! info "HPC-Centric Containers"
    - [Singularity a Container for HPC](https://www.admin-magazine.com/HPC/Articles/Singularity-A-Container-for-HPC)
    - NERSC Shifter
    - podman


## KRG Containers


### Development Environment
- base: [image](https://hub.docker.com/repository/docker/koresearch/develop-environment-base/general)/[source](https://gitlab.com/ko_research_group/software/dockerfiles/-/blob/main/develop-environment/cpu/Dockerfile?ref_type=heads)
### Applications
- Quantum ESPRESSO: [image](https://hub.docker.com/r/koresearch/qe)/[source](https://gitlab.com/ko_research_group/software/dockerfiles/-/blob/main/software-containers/quantum-espresso/Dockerfile?ref_type=heads)
