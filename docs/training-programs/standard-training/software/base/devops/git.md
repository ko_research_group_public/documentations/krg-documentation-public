# Git Version Control System

## KRG In a Nutshell

### Basic Concept
- version control system
- distributed (there are as many repositories as one clones)

### Basic Commands
- git add
- git commit
- git push
- git pull
- git branch
- git checkout
- git clone
- git log


!!! tip "One-time configurations"
    As a new git user, there are a number of one-time configurations to set:

    - identity (replace `John Doe` and `johndoe@example.com` to your identity in the following shell commands, see [more details (e.g., default text editors) here](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)):
    ``` bash
    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com
    ```

    - divergent branch handling (when `git pull`, typically one would go with the "merge" approach, see commandline hint for other options):
    
    ``` bash
    git config pull.rebase false
    ```

### Small Project (for Group Members)

- use `git` to add your member information (picture and short bio) to `docs/notes/people/KRG/`

### Further Reading

- [How to Write a Git Commit Message](https://cbea.ms/git-commit/)
- [Git Setup](https://swcarpentry.github.io/git-novice/02-setup.html)
- [removing history (dangerous)](https://stackoverflow.com/questions/9683279/make-the-current-commit-the-only-initial-commit-in-a-git-repository)

## Reading List

- [Chapter 5 Source code control through Git, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf) and [Tutorial](https://web.corral.tacc.utexas.edu/CompEdu/pdf/stc/hpc_git.pdf)



<!--=== "Tutorial Summary"

=== "Tutorial (with Bharatha, 2023-11-02)"
    `Git` is a version control system that allows you to keep track of a set of files (e.g., source code of a program) during the development process.
    It has a powerful feature that allows you to go to a previous version or merge different branches that when multiple people develop simultaneously.
    There has been several web services that augments around `git`, including GitLab and GitHub.

    Let's do an example by adding a `git` documentation page to KRG Documentation, which is written in MarkDown.
    Here, `#` represents the major heading and `## ` for the secondary heading.

    To use `git`, you will need probably around eight commands as listed [above](#basic-commands).
    After creating a files to be tracked in the repository, we will add this file by `git add <file>`.
    Then, `git commit` allows us to write the change to the history of the current repository (we will need to add some commit message).

    Here, current repository means my local repository; however, this commit is not yet present in this remote repository because they are separate repositories (since `git` is a *distributed* version control system.
    To synchronize the remote repository, we will need to `git push`; doing so allows us to send current repository that's local to the remote repository.

    For people who are unfamiliar with `git`, `GitLab` offers an easier route via its web interface.
    By directly editing the page on the website, one can easily make the commit via the web GUI.
    After we did this new update, the remote (Gitlab) repository is now more up-to-date than my local repository.
    We can then do a `git pull` on the local repository to bring it up-to-date with remote.-->
