# GitLab
!!! info "GitLab is a DevOps software package"

- [Set up SSH key](https://docs.gitlab.com/ee/user/ssh.html)

!!! info "see also"
    [`ssh` documentation page](../unix-like-systems/ssh.md)

- [Cloning a repo (using KRG-Doc as an example)](https://drive.google.com/drive/folders/1OGDk-LdppFDVQENH1c0uqXV_-XuLJnqv?usp=drive_link)


???+ note "add custom domain"
    - [GitLab Documentation](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/)
    - KRG domain hosted via [squarespace](https://account.squarespace.com); sign in and go to domain settings
    - Use the `docs` settings as an example to set up `CNAME` and `TXT` entries


!!! todo "Agile software development"
    - using [epic](https://docs.gitlab.com/ee/user/group/roadmap/)

