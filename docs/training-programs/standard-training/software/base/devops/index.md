# Standard Training: DevOPs

!!! abstract "[Containers (Docker)](container.md)"

!!! abstract "[`git`](git.md)"

!!! abstract "[GitLab](gitlab.md)"
