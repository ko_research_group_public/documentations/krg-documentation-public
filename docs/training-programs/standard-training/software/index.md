# Standard Training: Software

??? info "Excellent and Free Textbook by Victor Eijkhout: ["The Art of HPC"](https://theartofhpc.com/)"
    Use [KRG pdf cache (on 2024-01-08)](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/) if referred chapters/sections do not match the current version
    !!! tip "HPC Carpentry"
        Volume 4 has a good coverage of technical skills needed by our group

## Base Environment

??? tip "Set up KRG Program Environment (docker) if you don't have a unix-like system"
    To practice the remainder of this training, you will need a unix-like system with a number of packages installed.

    Follow [Level 1 Docker tutorial](base/devops/container.md#level-1-using-images) to setup KRG Program Environment ([koresearch/develop-environment-base](https://hub.docker.com/r/koresearch/develop-environment-base)), which is a cross-platform solution for MS Windows, Mac OSX, and Linux.

    !!! tip "keeping configurations"
        If you chose to use this container approach, you might find it handy to mount a host folder to `/root` in the guest (container file system) so that you can retain the settings (e.g., git configurations)

        !!! danger "use an unimportant folder to share documents with containers"
            The host folder should be dedicated for this training since docker runs as super user, it can delete important file from inside the container if the mounted volume (folder) is not isolated.

        !!! todo "Please report issues"
            If you spot any missing packages when using [koresearch/develop-environment-base](https://hub.docker.com/r/koresearch/develop-environment-base) to practice this training, please [open an issue](https://gitlab.com/ko_research_group/documentations/krg-doc/-/issues).

!!! abstract "[basic tools (link)](base/index.md)"

??? info "A wonderful tutorial series: ["The Missing Semester of Your CS Education"](https://missing.csail.mit.edu/)"
    !!! tip "Recommended Minimal Set in The Missing Semester Course"
        While the entire course is recommended, the most relevant topics are:
        
        - [Course overview + the shell](https://missing.csail.mit.edu/2020/course-shell/)
        - [Shell Tools and Scripting](https://missing.csail.mit.edu/2020/shell-tools/)
        - [Editors (Vim)](https://missing.csail.mit.edu/2020/editors/)
        - [Data Wrangling](https://missing.csail.mit.edu/2020/data-wrangling/)
        - [Command-line Environment](https://missing.csail.mit.edu/2020/command-line/)
        - [Version Control (Git)](https://missing.csail.mit.edu/2020/version-control/)

??? note "optional resources"
    [Software Carpentry Lessons](https://software-carpentry.org/lessons/)

??? question "checklist"
    - [ ] a running [koresearch/develop-environment-base](https://hub.docker.com/r/koresearch/develop-environment-base) docker container in your machine
    - [ ] basic unix-like system operations
    - [ ] basic `git`
    - [ ] basic `vim`

## Programming

!!! note "Textbook Reading"
    [Part I Introduction, Vol 3 of "The Art of HPC"](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol3/EijkhoutIntroSciProgramming-book.pdf)

!!! abstract "Prototyping: quick development for testing ideas"

    !!! tip "Recommedations"
        - [python](programming/python.md)

        ??? info "some other options"
            - octave/matlab
            - [Mathematica](programming/mathematica.md)
            - julia (also high-performance language)

        ??? warning "keep the test examples simple"
            Most languages good for prototyping may not offer optimal performance and/or scalability.


!!! abstract "High-Performance Computing: performance and scalability for production"

    !!! tip "Recommedations"
        - [Fortran](programming/fortran.md)
        - [C/C++](programming/c++.md)

        ??? info "strategy" 
            It is recommended to know both for KRG objectives.
            For those not fluent in C/C++, start with modern Fortran (e.g., Fortran2008).

        ??? tip "begin with a prototype"
            High-performance languages are often more involved to write than prototyping ones.
            Always start with a prototype to test correctness against.

        ??? tip "use a build system for compilation"
            Fortran and C/C++ codes need to be compiled, it is worth learning build systems to automate the procedure:

            - [Make](programming/build-system/make.md)
            - [CMake](programming/build-system/cmake.md)

## Domain-Specific Software

!!! todo "scientific software"
    move QE and LAMMPS to `scientific-software/` folder

!!! info "Electronic Structure"

    - [Quantum ESPRESSO](computational-chemistry/quantum-espresso/index.md)
    - [ ] FHI-aims

!!! info "Molecular Dynamics"

    - DeepMD-kit
    - LAMMPS

## HPC Cluster Operation

??? info "Additional Training for Using HPC Clusters"
    - [Slurm](base/unix-like-systems/slurm/index.md)

## Other Information

??? note "Other Software"
    - MS Teams + Planner
    - freeplane


??? info "KRG Software Repository"
    - [Group Source Repository](https://gitlab.com/ko_research_group/software)
    - [Dockerhub registry](https://hub.docker.com/u/koresearch)

