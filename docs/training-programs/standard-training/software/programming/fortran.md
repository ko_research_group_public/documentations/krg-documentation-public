# Programming: Fortran

!!! note "Textbook Reading"
    [Part III Fortran, Vol 3 of "The Art of HPC"](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol3/EijkhoutIntroSciProgramming-book.pdf)
