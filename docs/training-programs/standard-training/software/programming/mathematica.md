# Wolfram Mathematica (MMA)

!!! info "Mathematica (MMA)"
    MMA is a software system with strength in analytical analysis and many other features.
    While MMA is proprietary, members of UNT can [freely access it via the university subscription](https://aits.unt.edu/software/mathematica).

    Official tutorials: 
    
    - [Hands-on Start to Mathematica](https://www.wolfram.com/wolfram-u/courses/wolfram-language/hands-on-start-to-mathematica-wl005/)
    - [Fast Introduction for Math Students](https://www.wolfram.com/language/fast-introduction-for-math-students/)
