# Programming: Python

!!! info "Textbook Reading"
    [Al Sweigart, "Automate the Boring Stuff with Python"](https://automatetheboringstuff.com/) or [KRG pdf cache (on 2024-01-11)](https://github.com/ko-group/automate-the-boring-stuff-with-python/tree/krg-docs-cache)

!!! info "Useful Packages"
    - numpy
    - scipy
    - matplotlib
