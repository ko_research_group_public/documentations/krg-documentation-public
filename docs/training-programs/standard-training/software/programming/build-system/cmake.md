# CMake Build System

!!! info "What is CMake and why do I want to use it?"
    See [an introduction to modern CMake](https://cliutils.gitlab.io/modern-cmake/)

## KRG In a Nutshell
!!! todo "TODO"

## Reading List

- [make build system](make.md)
- [Chapter 4 The Cmake build system, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf) and [Tutorial](https://web.corral.tacc.utexas.edu/CompEdu/pdf/stc/hpc_cmake.pdf)
