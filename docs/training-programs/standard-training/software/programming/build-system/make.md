# Make Build System

## KRG In a Nutshell
!!! todo "TODO"

## Reading List

- [Chapter 3 Managing projects with Make, "Tutorials for High Performance Scientific Computing (The Art of HPC)", Vol 4, KRG-Cache](https://github.com/ko-group/TheArtofHPC_pdfs/blob/krg-docs-training-version/vol4/EijkhoutHPCtutorials.pdf) and [Tutorial](https://web.corral.tacc.utexas.edu/CompEdu/pdf/stc/hpc_cmake.pdf)
