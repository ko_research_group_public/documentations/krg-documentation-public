# VESTA

!!! abstract "VESTA is a 3D visualization package of crystal, volumetric and morphology data"
    - [VESTA website](https://jp-minerals.org/vesta/en/doc.html)

## Reading List

- VESTA papers and manual (browse) in [KRG Zotero Group Library](https://www.zotero.org/groups/5344838/krg/collections/RS4B472Y/items/FIIK8YD9/collection)
