# Open Babel: `obabel`

!!! info "[Open Babel](http://openbabel.org/)"
    A chemical toolbox designed to speak the many languages of chemical data
    - [list of supported formats](https://open-babel.readthedocs.io/en/latest/FileFormats/Overview.html)
    - simple use case:
    ```bash
    obabel [-i input-type] infile [-o output-type] -O outfile
    ```

!!! example "convert pdb to fhi-aims format"
    ```bash
    obabel -i pdb input.pdb -o fhiaims -O output.aims
    ```
???+ tip "side step slow ring analysis"
    When converting large structure, one may experience unexpected poor performance. This could be due to underlying (potentially unnecessary) ring analysis. To see if this is the problem, one can attach the running obabel process with `gdb`.
    ??? example "example output of `gdb -p `pgrep obabel` when executing `backtrace` (`bt`)"
        ```
        #0  0x00007f3292977533 in OpenBabel::operator==(OpenBabel::OBBitVec const&, OpenBabel::OBBitVec const&) () from /usr/lib64/libopenbabel.so.7
        #1  0x00007f3292a6fbed in OpenBabel::OBRingSearch::SaveUniqueRing(std::deque<int, std::allocator<int> >&, std::deque<int, std::allocator<int> >&) () from /usr/lib64/libopenbabel.so.7
        #2  0x00007f3292a70807 in OpenBabel::OBRingSearch::AddRingFromClosure(OpenBabel::OBMol&, OpenBabel::OBBond*) () from /usr/lib64/libopenbabel.so.7
        #3  0x00007f3292a71350 in OpenBabel::OBMol::FindSSSR() () from /usr/lib64/libopenbabel.so.7
        #4  0x00007f3292a2d855 in OpenBabel::OBMol::PerceiveBondOrders() () from /usr/lib64/libopenbabel.so.7
        #5  0x00007f328f72bf6e in OpenBabel::FHIaimsFormat::ReadMolecule(OpenBabel::OBBase*, OpenBabel::OBConversion*) () from /usr/lib64/openbabel/3.1.1/fhiaimsformat.so
        #6  0x00007f3292a8b9ce in OpenBabel::OBMoleculeFormat::ReadChemObjectImpl(OpenBabel::OBConversion*, OpenBabel::OBFormat*) () from /usr/lib64/libopenbabel.so.7
        #7  0x00007f3292a33805 in OpenBabel::OBConversion::Convert() () from /usr/lib64/libopenbabel.so.7
        #8  0x00007f3292a3a9b2 in OpenBabel::OBConversion::Convert(std::basic_istream<char, std::char_traits<char> >*, std::basic_ostream<char, std::char_traits<char> >*) ()   from /usr/lib64/libopenbabel.so.7
        #9  0x00007f3292a3cc86 in OpenBabel::OBConversion::FullConvert(std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >&, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >&, std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >&) ()   from /usr/lib64/libopenbabel.so.7
        #10 0x0000564b6c5e86bb in main ()
        ```
        In this example, one can see `OpenBabel::OBRingSearch::SaveUniqueRing` is the main source of slow down if one sample a few time frame.
    ???+ tip "solution"
        To solve this problem, one can simply side step ring analysis (if not necessary) by adding input options, e.g.:
        ```bash
        # obabel -ifhiaims system.aims -ab -opdb -O system.pdb # this is the slow version
        obabel -ifhiaims system.aims -opdb -O system.pdb # this is the fast version
        ```
        In this case, `-a` is for [adding input options](https://open-babel.readthedocs.io/en/latest/Command-line_tools/babel.html) and [`b`: Disable bonding entirely](https://open-babel.readthedocs.io/en/latest/FileFormats/FHIaims_XYZ_format.html) added to `-a`
