# Quantum ESPRESSO (QE) Valence Electron / Pseudopo-Wavefunction Specification

## Relevant Input Parameters

See the following [pwscf input parameters](https://www.quantum-espresso.org/Doc/INPUT_PW.html):

- `ecutwfc` in the `system` namelist
- (optional) `input_dft` in the `system` namelist
- (optional) `vdw_corr` in the `system` namelist
- (optional) `use_sea` in the `system` namelist
