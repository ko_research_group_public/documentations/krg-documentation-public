# Compiling Quantum ESPRESSO

!!! tip "use CMake"
    We prefer the `CMake` approach specified by [this document](https://gitlab.com/QEF/q-e/-/wikis/Developers/CMake-build-system)

## TLDR (short story)
``` bash
# in the q-e root folder
cmake -B build
cd build
make pw

```

```bash
$ git clone git@gitlab.com:QEF/q-e.git #for Clone q-e files into your directry.
$ cd q-e
$ sudo snap install cmake  # version 3.28.1, or
$ sudo apt  install cmake  # version 3.27.4-1 recommended
$ sudo apt install build-essential gfortran #install CMAKE_Fortran_COMPILER 
$ sudo apt install mpich
$ sudo apt install libblas-dev liblapack-dev #complete set of external BLAS/LAPACK library
$ sudo apt install fftw-dev # install external FFTW library.
$ cmake -DQE_FFTW_VENDOR=Internal -B build #enable reference FFTW at a performance loss compared to optimized libraries
```

