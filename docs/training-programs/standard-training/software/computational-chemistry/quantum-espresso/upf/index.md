# Unified Pseudopotential Format (UPF)

!!! info "documentation page"
    [see this page](https://pseudopotentials.quantum-espresso.org/home/unified-pseudopotential-format)

## Relevant Input Parameters

See the following [pwscf input parameters](https://www.quantum-espresso.org/Doc/INPUT_PW.html):

- `pseudo_dir` in the `control` namelist
- `ntyp` in `system` the namelist
- `ATOMIC_SPECIES` card

## Databases

Some good source include the [SG15 database](http://www.quantum-simulation.org/potentials/sg15_oncv/upf/) and [pseudo-dojo](http://www.pseudo-dojo.org/).

???+ tip "Track Sources of Pseudopotentials"
    If you use existing database, you can use a script (e.g., in a `get-pseudo.sh` file in your calculation folder) to record the sources as follows:
    ```shell
    #!/bin/bash
    wget http://www.quantum-simulation.org/potentials/sg15_oncv/upf/N_ONCV_PBE-1.2.upf  # SG15, N
    wget http://www.quantum-simulation.org/potentials/sg15_oncv/upf/C_ONCV_PBE-1.2.upf  # SG15, C
    wget http://www.quantum-simulation.org/potentials/sg15_oncv/upf/Si_ONCV_PBE-1.2.upf  # SG15, Si
    wget http://www.quantum-simulation.org/potentials/sg15_oncv/upf/O_ONCV_PBE-1.2.upf # SG15, O
    wget http://www.quantum-simulation.org/potentials/sg15_oncv/upf/H_ONCV_PBE-1.2.upf # SG15, H
    wget http://www.quantum-simulation.org/potentials/sg15_oncv/upf/Al_ONCV_PBE-1.2.upf # SG15, Al(11)
    wget http://www.pseudo-dojo.org/pseudos/nc-sr-05_pbe_stringent/Al.upf.gz # pseudo-dojo ONCV Al(3)
    ```

???+ warning "SG15 does not come with pseudowavefunctions"
    When performing `projwfc.x` calculations that requires pseudowavefunctinos (e.g., for the projected density of states or the Lowdin charges), SG15 pseudopotentials will give you an error message similar to: "Cannot project on zero atomic wavefunctions!". [Here is a related thread](https://www.mail-archive.com/users@lists.quantum-espresso.org/msg41342.html).

    Solution: use other pseudopotentials that contains pseudowavefunctinos (i.e., non-empty `PP_PSWFC` section in the UPF file), e.g., from [pseudo-dojo](http://www.pseudo-dojo.org/).
