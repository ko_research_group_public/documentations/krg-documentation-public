# Quantum ESPRESSO

!!! todo "link pages in this folder..."

## External Training Materials
- ["A Gentle Introduction to DFT Calculations"](https://www.materialscloud.org/learn/sections/VNL7RL/a-gentle-introduction-to-dft-calculations-april-2020)
- [Quantum ESPRESSO Schools](https://www.materialscloud.org/learn/sections/VLoB41/quantum-espresso-schools)
- [An Open Computational Materials Physics Course Using QE](https://www.compmatphys.org/)
- [MaX Webinar "How to Use Quantum ESPRESSO on New GPU-based HPC Systems](https://www.max-centre.eu/webinar/how-use-quantum-espresso-new-gpu-based-hpc-systems)

## PWSCF
- SeA (TODO)

???+ warning "avoid non-ascii characters"
    input processing may fail for non-ascii characters. Also check for tab's (\t)...


## FAQs

??? question "What are the many files named `pwscf.wfc*` and `pwscf.mix*` left in the directory?"
    Those are scratch files (`pwscf.wfc*` are wavefunction coefficients and `pwscf.mix*` are SCF mixing parameters) from each MPI process. These files typically get removed automatically at the end of the calculation. When you see them after your slurm job stops running, it typically means that your calculation did not finish (probably due to insufficient wall time limit or other reasons).
    
    If the calculation did finish, there is no need to keep those files. However, if the calculation did not finish (check output file), one should analyze the input, output, and submission script.