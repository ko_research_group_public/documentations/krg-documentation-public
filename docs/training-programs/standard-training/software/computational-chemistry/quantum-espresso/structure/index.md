# Quantum ESPRESSO (QE) Strcuture Specification

!!! info "documentation page"
    [see input parameters](https://www.quantum-espresso.org/Doc/INPUT_PW.html)

## Relevant Input Parameters

See the following [pwscf input parameters](https://www.quantum-espresso.org/Doc/INPUT_PW.html):

### Unit Cell

- `ibrav` in `system` the namelist
- `celldm` or {`A`, `B`, `C`, `cosAB`, `cosAC`, `cosBC`} in `system` the namelist
- `CELL_PARAMETERS` card
- `K_POINTS` card

### Atoms in the Cell

- `nat` in `system` the namelist
- `ATOMIC_POSITIONS` card
