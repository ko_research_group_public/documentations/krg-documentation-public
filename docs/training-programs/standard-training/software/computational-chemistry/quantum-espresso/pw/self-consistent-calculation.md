# Quantum ESPRESSO (QE) PWSCF: SCF - Self-Consistent Field Calculation

- run `pw.x` with `calculation` set to "scf"

???+ info "documentation page"
    [see input parameters](https://www.quantum-espresso.org/Doc/INPUT_PW.html)

???+ tip "useful references"
    - review: [Woods, Payne & Hasnip, "Computing the self-consistent field in Kohn–Sham density functional theory" JPCM 31, 453001 (2019).](https://iopscience.iop.org/article/10.1088/1361-648X/ab31c0)
    - a new preconditioner: [Herbst and Levitt, "Black-box inhomogeneous preconditioning
for self-consistent field iterations in density
functional theory" JPCM 33 (2021) 085503](https://doi.org/10.1088/1361-648X/abcbdb)

???+ tip "Pseudopotentials"
    [see this documentation page](../upf/index.md)

???+ tip "Dealing with difficult convergence"
    - visualize input structure (e.g, using [xcrysden](http://www.xcrysden.org/)) and check if everything is reasonable (structure far out-of-equilibrium are typically difficult to reach SCF)
    - add some smearing (typically set smaller than the band gap for insulators)
    - use a (aggressively) small mixing parameter (e.g, `mixing_beta` set to 0.1 or smaller)

## Minimum Dependencies

- [Quantum ESPRESSO (QE) Strcuture Specification](../structure/index.md)
- [Pseudopotential / Unified Pseudopotential Format (UPF)](../upf/index.md)
- [Valence Pseudopo-Wavefunction Specification](../electrons/index.md)

## Examples

??? example "single water molecule"
    a working example can be found [here](https://gitlab.com/ko_research_group/research/research-notes/-/tree/main/docs/hsin-yu.ko/2024-01-30/h2o?ref_type=heads)
    ``` fortran
    &control
       calculation = 'scf'
       pseudo_dir  = './'
    /
    &system
       ibrav     = 1
       a         = 15.0
       nat       = 3
       ntyp      = 2
       ecutwfc   = 85.0
       !input_dft = 'pbe0'
       !vdw_corr  = 'TS'
       !use_sea   = T
    /
    &electrons
    /
    ATOMIC_SPECIES
     O 16.0d0 O_ONCV_PBE-1.2.upf
     H 1.00d0 H_ONCV_PBE-1.2.upf
    ATOMIC_POSITIONS {bohr}
       O     0.0099    0.0099    0.0000
       H     1.8325   -0.2243    0.0000
       H    -0.2243    1.8325    0.0000
    K_POINTS {gamma}
    ```

