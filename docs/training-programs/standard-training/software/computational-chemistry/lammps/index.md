# LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator.

!!! abstract "A molecular dynamics program from Sandia National Laboratories"
    - [LAMMPS official tutorials](https://www.lammps.org/tutorials.html)

## Reading List

- [LAMMPS manual](https://docs.lammps.org/Manual.html)
- LAMMPS papers in [KRG Zotero Group Library](https://www.zotero.org/groups/5344838/krg/collections/CXUCZBTV)
