# KRG Code of Conduct (Expectations)

!!! success "Core Mission: Collective Success"

    - Push the boundary of computational condensed-phase chemistry
    - Maintain steady stream of research funds
    - Prepare our trainees for pursuing their career goals ([realign the objectives with members annually](research/review/annual.md#objectives))

    The KRG code of conduct provides a set of guidelines regarding how we pursue this collective success.

??? quote "High achievement always takes place in the framework of high expectation.<br/> --Charles Kettering"
    We set the following expectations in addition to [UNT Code of Student Conduct](https://policy.unt.edu/policy/07-012) and [other policies](https://policy.unt.edu/).

## General Expectations

=== "Research"

    ??? success "Openness & Integrity"
        Scientific claims must be significant and testable. KRG aims for maximum openness
        in sharing research details to support our published claims (e.g., using public repositories).

        We aim to perform research with a high level of organization (e.g., maintaining reproducibility by
        version-controlled scripting and detailed documentation) as well as maintaining strict scientific integrity.

        KRG members want to proudly share your data/scripts to support your published claims to anyone.
        As such, we regularly organize and arcive research projects (document and store everything if possible).
        Even for repeated studies, keep them in separate folders for later comparison.

        See also: [UNT RCR](https://research.unt.edu/research-services/research-integrity-and-compliance/responsible-conduct-research-rcr)

        !!! note "[Notes to HYK] Responsible Conduct in Research (RCR) Course (COS 5390.001)"
            The Responsible Conduct of Research (RCR) for UNT graduate students and post-docs (typically there will be a reminder email from the Dean before a Semester starts)

    ??? success "Diligence, Efficiency & Focus"
        We believe efficient and hard work are necessary to become successful while passion and fun are the fuel for maintaining the effort.

        While KRG does not set rigid work hours, we expect our members to work as hard as possible within the limit
        that does not sacrifice a healthy life (both physically and mentally). Early successes open up many doors for
        your career.

        It is also worth noting that **focus** strongly improves efficiency.
        As such, we expected full time commitment from our members; please discuss with [Hsin-Yu](mailto:hsin-yu.ko@unt.edu) about any concerns if you need special accomodation.

    ??? success "Independence & Perseverence"
        KRG aims to train independent researchers and persevering problem solvers.
        When a problem appears, we ask our members to take a moment and first try to solve it (e.g., via searching in the web/literature/KRG documentation and looking at the error message).

        If your effort was successful, you learn it better than asking (please add a note in the KRG documentation system in case the issue has not been previously addressed).
        If it did not work out (say after ~30 minutes), ask a group member or [Hsin-Yu](mailto:hsin-yu.ko@unt.edu) for help.

        !!! tip "Be Patient and give context"

    ??? success "Leverage Available Resources"
        Use available resources (e.g., computer time, university resources, and technical support) to thrust your research as efficiently as possible.

        !!! tip "Expect mistakes and learn from them"
            There will be occational waste in resources and that is totally expected.
            As long as they get fix quickly and documented clearly (to avoid repeatition), mistakes are also considered part of the research progress.

=== "Communication"

    ??? success "Early"
        Communicate at the earliest moment possible (e.g., planning project/collaboration/conference, requesting reference letters, and revising career plans)

    ??? success "Clear & Honest"
        When communicating, use the most precise wording as you can.
        We all make mistakes (a lot of them); there is no need to worry about reporting failed attempts (even if it may seem sily). This will allow KRG to better train you.

    ??? tip "Don't be afraid to bring up difficult conversations"
        Difficult conversations need to be dealt with; it is typically less painful to work on it early.
        Hsin-Yu is here to help you.

    ??? tip "Succinctness vs. Verbosity"
        For beginning members, high verbosity is encouraged to reduce communication gap.
        As one becomes familiar with KRG group culture, succinct communication is preferred to maximize efficiency.


=== "Management"

    ??? success "Be Responsible for What You Want"
        When pursuing an objective (e.g., Ph.D. degree, fellowship application), it is assumed that you will be responsible for the management, including, e.g.:

        - organize all requirements (ideally in a simple and clear manar to avoid potential issues)
        - send reminders well before the deadline (ideally: months ahead)
        - ensure all requirements are met

    ??? success "Report Effort (Billable Hours)"
        Provide time management information regularly (biweekly or monthly) to allow analysis of research effort (required when working on sponsored project, e.g., RA)


=== "Work Environment"

    ??? success "Safe, Supportive & Inclusive Environment"
        KRG is a collaborative group. It is important to be respectful and patient to each other, especially for members from underrepresented backgrounds.

        We understand members come from various backgrounds and have exposures to different area of science.
        KRG is a safe environment to acquire knowledge: No one will be blamed for asking naive questions. It is most efficient to identify and address knowledge gaps as early as possible.

        We encourage our members multiply their impact to junior members by contributing to this documentation system so that you won't need to repeat your self.
        We also ask our members to appropriately credit help/assistance that you received from others.

    ??? success "Health and Well-Being"
        Doing science is a mental (and sometimes physical) challenge.
        Maintaining good health boost the productivity and the fun of scientific research.

        Eating good foods, exercising regularly, and sleeping well can make a big difference to your scientific career and personal life.
        You are welcome to talk to [Hsin-Yu](mailto:hsin-yu.ko@unt.edu) if you need some additional tips.

## Role-Specific Expectations

=== "PI"
    ??? abstract "Vision"
        - Provide big-picture vision of the scientific focus of KRG

    ??? abstract "Running the Group"
        - Obtain resources (e.g., funding and computer) to support the research and trainees
        - Meet trainees regularly to discuss research

    ??? abstract "Mentorship"
        - Assist trainee overcome hurdles to make scientific progress (by playing devil's advocate and challenge the science)
        - Support trainee's career development (e.g., letters of recommedation and conference travels)

=== "Postdocs"

    ??? abstract "Develop a Career Development Plan"
        - what is the next career step
        - plan to achieve
        - contingency plan
        - meet regularly with Hsin-Yu to refine the plan 

    ??? abstract "Learn to Become a PI"
        - develop independent vision
        - participate in running the KRG group (e.g., grant writing)
        - gain experience mentoring graduate student

=== "Graduate Students"

    ??? abstract "Manage Program Timeline"
        - schedule annually time with Hsin-Yu to refine the plan
        - execute the plan to meet program requirements on the appropriate timeline
            
## False Expectations

??? danger "Do not expect mentors to execute your plan"
    Your mentors are expected to help you set a plan (e.g., schedule Ph.D. defense), but you will be the person in charge of executing it.

    **Mentors are typically oversubscribed and may not always recall your milestone dates; please schedule the events using calendar invites and kindly remind them before hand.**

??? danger "Short notice will not always work"
    While mentors will strive to accommodate emergent/urgent requests, availability cannot be guaranteed at short notice. The definition of short notice depends on the complexity of the task, e.g., it might be good to give people at least a couple of days before a requested research meeting, while it will typically take more than a couple of weeks for people to get to a reference letter.



*[PI]: Principal Investigator
