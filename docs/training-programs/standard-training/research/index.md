# Standard Training: Research

???+ info "Elements of Research"
    ???+ question "A Significant Problem"
        an open question relevant to our objectives

    ???+ abstract "A Solution"
        a proposed (ideally simple and general) solution to the problem

    ???+ success "Evidences"
        supportive case studies and physical/chemical intuitions to showcase the effectiveness of the solution 

        ??? question "Evidences include:"

            - proof-of-principle case: a simple (ideally simplest possible) and representative case study that shows the solution solves the problem (keeping everything else fixed)
            - facts: a limiting condition with an analytical solution or an experimental observable sensitive to the introduction of the solution
            - scope: a variety of systems to show wide applicability

???+ question "How to approach a scientific question/problem?"
    - develop a hypothesis (anticipatory answer/solution to the question/problem)
    - design (computational) experiments to test/prove the hypothesis
    - repeat previous steps until hypothesis passes several (say ~3) independent tests
    - develop a story from the hypothesis to make it a theory (as well as a manuscript that delivers (ideally) a single and coherent message)

## Topics

- [Reference Management](reference-management.md)
- [Library Resources](library-resources/index.md)
- Planning
- Presentation

## Tips

??? tip "Keep it simple"
    - [Occam's razor](https://en.wikipedia.org/wiki/Occam%27s_razor)
    - minimize mistakes
    - easy to understand, interpret, and present
