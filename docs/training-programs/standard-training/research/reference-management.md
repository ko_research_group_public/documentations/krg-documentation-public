# Reference Management

## Accessing Library Subscription Services

- [UNT Library Proxy](https://library.unt.edu/proxy-tools/)

## Reference Manager

Our group choose [Zotero](https://www.zotero.org/) as our primary reference manager. See also [UNT Library's discussion regarding reference managers](https://guides.library.unt.edu/referencemanagers/home).

!!! info "KRG Zotero Group"
    [Zotero groups](https://www.zotero.org/support/groups) allows collaborative library.
    [Link to our Zotero group page](https://www.zotero.org/groups/5344838/krg/library)

