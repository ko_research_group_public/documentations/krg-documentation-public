# UNT Library Resources
- [Graduate Student Services](https://library.unt.edu/services/for-graduate-students/)
- Articles
- [Databases](https://guides.library.unt.edu/az.php)
- [Government Information](https://guides.library.unt.edu/gic)
- [Data Resources](https://guides.library.unt.edu/data)
- [Inter Library Loan](https://iii.library.unt.edu/) (ILLiad)
- Document Delivery
- [Find items in libraries near you](https://search.worldcat.org/)
- Printing, Scaning, Photocoping
- Lockers and Study Spaces
- Writing Help
- Scholarly Communications services
- Workshops
- [New Purchase Request/Recommendation](https://library.unt.edu/forms/new-purchase-requestrecommendation/)
- [Library Proxy Links (Paywall Utilities)](https://library.unt.edu/proxy-tools/)

Guide for ["What UNT Libraries can do for you"](https://guides.library.unt.edu/what-UNT-Libraries-can-do-for-you)
