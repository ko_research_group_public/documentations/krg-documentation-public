# Annual Review

## Objectives

!!! abstract "Trainee Career Goals"
    What are your plans for (each within five sentences):

    - next semester
    - next year
    - next five years

## Performance

!!! abstract "Productivity"
    
    - Program Requirements
    - Projects and Manuscripts
    - Journal Articles
    - Conference Presentations

!!! tip "Keep Update Your CV Updated."
    Whenever there is a change, update your CV so that you don't need to spend time recall relevant events.
