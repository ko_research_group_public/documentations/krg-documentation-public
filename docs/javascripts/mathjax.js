window.MathJax = {
  loader: {load: ['[tex]/mhchem']},
  tex: {packages: {'[+]': ['mhchem']}}
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  }
};

document$.subscribe(() => {
  MathJax.startup.output.clearCache()
  MathJax.typesetClear()
  MathJax.texReset()
  MathJax.typesetPromise()
})
