# External Resources

- [Blogs](blogs.md)

## Documentation

- [Grammarly](https://app.grammarly.com/)
- [Documentation Page of Material for MkDocs](https://squidfunk.github.io/mkdocs-material/reference/)
- Overleaf

## HPC Resources

- [CASCaM Center at UNT Chemistry](https://chemistry.unt.edu/~cruntch/)
- [NERSC Documentation](https://docs.nersc.gov/)
- [Cornell Center of Advanced Computing (CAC) Virtual Workshop](https://cvw.cac.cornell.edu/topics)
- [IDEAS Community](https://ideas-productivity.org/index.html)

## Online Database

- [Cambridge Crystallographic Data Centre (CCDC)](https://www.ccdc.cam.ac.uk/structures/?)
- [Reaxys](https://www.reaxys.com)

<!--## Business Promotion
??? "Name Card Majic"
    [Halftone QR Code](https://nythrox.github.io/halftone-qrcode/)-->
