# External Blogs

???+ example "Terence Tao's Blog"
    - [blog site](https://terrytao.wordpress.com/)
    - target: math graduate students (but also useful for general STEM students)
    - contents:
        - [careers advice](https://terrytao.wordpress.com/career-advice/)
        - [mathematical writing](https://terrytao.wordpress.com/advice-on-writing-papers/)
        - expository articles (such as my articles for the Princeton Companion to Mathematics, or for the tricks wiki);
        - topic of Tao's research

