# Information for Prospective Students

## Ph.D. Program (Chemistry)

???+ tip "How are you going to be evaluated?"
    While we evaluate applicants holistically and individually, there are some characteristics that we seek:

    - Track Record

    Preferred (but not required) experiences include: (1) performing research in computational chemistry/physics/materials science and scientific software development;
    (2) authoring/co-authoring research publications; and (3) presenting at conferences.

    - Motivation

    Science is hard; making significant progress requires careful work, patience, and perseverance.
    A convincing graduate application should show strong motivation that drives the applicant.

    - Communication

    Language allows one to stand on the shoulders of giants; while perfect language skills are not required, basic proficiency is crucial.

### Curriculum Vitae (CV)

???+ info "CV is your **track record**."
    CV provides your professional identity and relevant qualifications.
    Be strategic; include relevant information that fits well with the position.

    Several good tutorials can be found online, e.g., [the Chemistry CV example on this page](https://careerservices.upenn.edu/application-materials-for-the-faculty-job-search/cvs-for-faculty-job-applications/).
    We note that even though the above example is for faculty job application, an ideal graduate student CV shares the same elements (albeit less extensive).

    Make sure to include all relevant current and previous education and training.

???+ tip "Know your audience."
    While a CV is expected to contain extensive information about your professional identity, one should avoid burying relevant qualifications underneath irrelevant details.
    When applying for KRG, for instance, it is great to mention experience in GNU/Linux, but it may strike as ignorant if one lists truck driving experience (and impedes the reader from continuing reading, leaving relevant details unnoticed).

    However, it is unnecessary to trim your CV to the bare minimum. A good strategy is to sort the information in your CV in descending relevance.


???+ example "Grade-Point Average (GPA)"
    GPA from your transcript typically provides a first impression of you even though GPA is not a universally comparable metric.
    As indicated by [the UNT Chemistry Admission FAQ page](https://chemistry.unt.edu/graduate-program/prospective-students/graduate-application-faqs), having a GPA greater or equal to 3.0 (4.0 scale) is preferable.

    For applicants with lower GPAs, it might be a good idea to explain why it will not impede you from pursuing your graduate study.
    For instance, mention if a lower GPA is common in your current/previous institution (e.g., the person with a GPA of 3.0 out of 4.0 ranks highly in your class).
    Another instance is that one performs very poorly in some courses entirely irrelevant to your graduate study (you can present another "specialty GPA" from the relevant ones along with your overall GPA).
    If you perform well in standardized tests (e.g., TOEFL, GRE, subject GRE), you may include this information to support your prospect of a successful graduate study.


### Statement of Purpose (SOP)

???+ info "SOP shows your **motivation** and **communication**"
    Distinct from the more structured CV, SOP writes your motivation for pursuing a Ph.D. degree as a computational/theoretical chemist in the KRG at UNT.

    See ["What is a Statement of Purpose?" in the UNT Chemistry FAQs](https://chemistry.unt.edu/graduate-program/prospective-students/graduate-application-faqs) for general guidelines.
    Another good resource is [this tutorial](https://grad.berkeley.edu/admissions/steps-to-apply/requirements/statement-purpose/).

    ???+ tip "For those who came from a different background, consider elaborate on the following items:"
        - Your motivation to pursue computational chemistry
        - How do you plan to leverage experience from your background?
        - (Optional) your previous experience with scientific computing (e.g., computational chemistry programs), programming (e.g., Python, Matlab, Mathematica, Julia, Fortran, C), or command line operations (e.g., Linux, Bash)
        - (Optional) your good command or wide exposure to college-level math and physics courses
        - (Optional) a specific scientific question that you want to pursue for which our group is uniquely suited to mentor you

???+ tip "Avoid grammatical and spelling errors."
    
    Online tools such as [Grammarly](https://app.grammarly.com/) can be helpful (the free edition should be good enough for most cases).


???+ tip "Include a couple of other faculty members with whom you would be interested in working"
    As a graduate student at UNT Chemistry, one will be asked to explore at least three groups before joining a research group.

### Reference Letters

???+ info "Reference letters should come from people who know you **professionally**"
    Ask letter writers well before the deadline (at least a month).
    Choose letter writers (if you have more than enough) based on their connection to your target, but don't worry if you don't have this choice.

    Send reminders well before the deadline (e.g., at one-week and three-day marks). Typically, the required reference letters have soft deadlines (as letter writers are almost always busy people).

???+ question "Correcting a mistake in the letter"
    While the content of a reference letter is not something you can have precise control over, occationally the letter writter wants to make typographical corrections to the uploaded document. 
    In this case, the letter writter can [upload the letter again](https://chemistry.unt.edu/forms/letter-recommendation-submission). It may be worth it to also notify [chemgrad@unt.edu](mailto:chemgrad@unt.edu) about this update.


### Some Tentative Interview Questions

We use interviews to assess your:

- research interest
- skill set
- motivation
- communication

???+ question "Research Interest"
    ???+ question "Can you tell me about your current and past research (within 10 minutes)?"
    
        It can be a plain whiteboard discussion or via slides (no more than three slides).

        You might want to discuss:
        
        - the significance (big picture) of the project (i.e., "Why should one care about this research?")
        - your main results
        - skills learned during the process (also helps us design training strategies for you)

    ???+ question "Do you have research interests that you want to pursue for your Ph.D. studies?"
        While there are existing research focuses at KRG, we always seek common ground to align our goals.
        It is okay if it is yet to be developed and you want to explore the field of computational condensed-phase chemistry (which is indeed an exciting and fast-growing field).

???+ question "Skill Set"
    ???+ question "What advanced math and physics courses have you taken?"
        On top of chemistry, our research benefits a lot from exposure in math and physics.

    ???+ question "What programming languages are you familiar with?"
        We use all kinds of tools from python, bash, Fortran, C/C++. Familiarity with any programming language will help you pick up the required tools.

    ???+ question "What are some of your experiences in scientific computing?"
        KRG is a research lab working on computational condensed-phase chemistry.
        We want to know your scientific computing experiences to help you get started quickly.

    - It is okay to start with little experience as a beginning Ph.D. student as long as one is open to actively learning new tricks.

???+ question "Motivation"

    ???+ question "What are your ambitions after finishing Ph.D. training at KRG? How can KRG help you achieve this goal?"
        It is important to plan for the future (even though we won't have absolute control).
        Knowing what you want helps us better prepare you for your ambitions.

???+ tip "It is okay to not have all the answers."
    The nature of Ph.D. studies is to push the boundaries of existing knowledge. These questions are conversation starters that help us to start thinking together.

???+ question "How did you learn from the opportunity at KRG?"
    This is a simple question for your feedback.

### Frequently Asked Questions

???+ question "How are graduate students supported at KRG?"
    Graduate students (Ph.D. program) will be supported via [teaching (TA) and research assistantships (RA)](https://tgs.unt.edu/future-students/funding/teaching-fellows-and-assistants).
    Both assistantships provide tuition coverage, standard graduate student benefits, and stipends.

    For beginning students, we plan to support them via TA so that they pick up relevant communication skills via teaching and research tools by taking necessary courses and training (beginner research projects).
    As the students become more experienced, we will shift to RA to help students better focus on research projects.

    Since we prefer students to focus on research, we are also working on getting more funds to support extra RA positions.

???+ question "What is the amount of the student stipends?"
    The stipend amount for a Ph.D. student depends on the type of funding and a number of factors (see [this link](https://vpaa.unt.edu/resources/retention)).
    While it is hard to provide the up-to-date exact number, we do have previous statistics (in the following links for [RA](https://vpaa.unt.edu/file/24427) and [TA](https://vpaa.unt.edu/file/12841)).
    In spite of the factors out of my control, my priority is to do everything I can to maintain the high morale of the group (higher stipend rate included).

???+ question "What are some scholarships that I am eligible for or can apply for?"
    At UNT Chemistry, there is a list of [available awards and scholarships](https://chemistry.unt.edu/graduate-program/current-students/awards-and-scholarships).
    UNT Chemistry automatically considers all applicants for any and all scholarships/awards for which they are eligible (see [What funding options are available for graduate students?](https://chemistry.unt.edu/graduate-program/prospective-students/graduate-application-faqs)). 

???+ question "What are some preferred skills and knowledge for working at KRG?"
    As a computational condensed-phase chemistry lab, KRG works at the interface of chemistry, physics, and scientific computing.
    One may find it handy to have knowledge of physical/computational chemistry, solid-state physics, materials simulation, applied mathematics, and high-performance computing.
    Valuable skills to develop during the process include: programming (e.g., bash, python, Julia, Fortran), command line text editing, electronic structure calculations, molecular dynamics, etc.

    While preferable, these knowledge and skills are not prerequisites. We are building this documentation system to provide standard training for new students to fill this gap.

    If you want to begin learning these skills, a good starting point is [familarize with unix-like systems](../../training-programs/standard-training/software/base/unix-like-systems/index.md).
    For scientific packages, it would be nice to gain familiarity with the [Quantum ESPRESSO package (particularly the `pwscf` module)](https://www.quantum-espresso.org/).

???+ question "What is the general graduate experience in your department in terms of collaborative settings, research tools, and professional growth?"
    As a Ph.D. student, your experience within the chosen research group will most likely dominate; as such, the answer is made specific to the KRG.

    Regarding the general graduate experience, we aim to tailor it based on the individual career goals of each student.
    Generally speaking, each student should lead a couple of projects and collaborate within and outside of KRG to gain various experiences and communication skills.
    The collaborative settings will be mostly at the UNT campus and potentially involve groups from other universities (e.g., Cornell, Temple, and Princeton) as well as people from national labs (e.g., LLNL and LBNL).

    As for research tools, we are building a KRG high-performance computing (HPC) cluster (GPU architecture) as part of the CASCaM Instituted at UNT; it should be ready by Fall 2024 and will be the main work environment for our students.
    The research skills acquired as computational scientists are also highly transferable to the tech industry, national labs, and finance; many of my peers are employed at Google, Facebook, and IBM, as well as major bank research sectors.

    Our group keeps the career goals of our students in mind when planning research projects and will have [annual reviews](../../training-programs/standard-training/research/review/annual.md) within our group to realign the career goals of our students.

???+ question "When should I submit my pre-application?"
    Generally, early submission is a good thing as it shows good planning and could lead to early admission (prior to the application deadline). While you can submit as early as you want, applying too early can lead to some risks (e.g., mistakes in categorization and underselling yourself). As such, a sweet spot is probably completing the submission (including reference letters and other required documents) about a month ahead of the deadline.


### External Resources

- [Required Materials for Ph.D. Program](https://chemistry.unt.edu/graduate-program/prospective-students)
