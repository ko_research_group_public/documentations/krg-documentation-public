# Time Management

!!! info "Tracking the Research Time"
    When you work on a sponsored project, funding agencies typically require Hsin-Yu to certify your research effort regularly.

    To comply this requirement (as well as better managing research effort in general), please record your research activities on your UNT outlook calendar as events.

    ??? tip "Suggested Format"
        - Event title: `[Project Name]`
        - Time window
        - Summary of activities in bullet points

    ??? tip "Monthly Report"
        - hours spent on each research project
        - complete list of summarized bullet points

!!! success "Maintain Good Health and Well-Being"
    Scientific research is fun (and hard) and typically induces (and requires) high commitment for one to become successful. (Many scientists do research whenever they can.)

    While we are all expected to be fully committed to doing research, it is not worth sacreficing health and well-being.
    The KRG time management policy is mainly to help improving efficiency and planning rather than pushing members beyond their limit.
