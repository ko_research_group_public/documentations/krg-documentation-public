# Meetings SOP

??? abstract "Elements of effective meetings:"
    !!! info "Purpose"
        include impact/significance on the agenda
    !!! info "Engagement"
        include the reason why each attendee is needed
    !!! info "Freedom and Clarity"
        [the chair] fosters a safe space for relevant and accurate information exchange
    !!! info "Records"
        share/update actual meeting time and action points (in calendar event)

    Source: ["A Formula for Perfect Research Project Meetings"](https://www.fasttrackimpact.com/post/2018/10/19/a-formula-for-perfect-research-project-meetings):

## Group Meeting

??? info "Weekly 100-Minute Meetings During Fall and Spring Semesters" 
    Scheduled prior to each semester by the Group Meeting Task Force and recorded in the admin spreadsheet; see [internal documentation page for this link](https://docs.ko-research.org/operating-procedures/meetings/) 

    - Avoid breaks, finals, and holidays
     
    !!! info "Speakers" 

        - Group members
        - Collaborators
        - External presenters
        - Tutorial sessions

        ??? tip "[Notes to HYK] external speakers"
            search "People to invite to Group Meetings" in outlook email note...

    !!! example "Agenda"

        - Administrative tasks/announcements (5 minutes)
        - Brief research update (5 minutes per group member, one slide)
        - A focused research report or journal presentation for newer students (50 minutes)
        - Discretionary time (10 minutes)
        - [ ] optional recording for tutorials

    !!! info "elements"
        !!! abstract "Purpose"
            - practice communication skills
            - organize research progress
            - learn new research directions and foster collaborations
            - logistics

        !!! abstract "Engagement"
            - everyone will be presenter and active audience

        !!! abstract "Freedom and Clarity"
            All questions and suggestions need to be communicated respectfully and are welcome during the meeting.

        !!! abstract "Records"
            All slides should be uploaded to the KRG Group Meeting Channel; see [internal documentation page for this link](https://docs.ko-research.org/operating-procedures/meetings/)

## On-Demand Meetings

??? info "60-Minute Research Meetings" 

    === "Prior to Fall 2024"

        Request via email based on [Hsin-Yu's Cornell Availability](https://outlook.office365.com/owa/calendar/6b093b1b90164c88821afb61ae6d0342@cornell.edu/5810cf34576745159874b5b2dfe497ec650800812157149748/calendar.html)

    === "Starting Fall 2024"

        Schedule with Hsin-Yu's Bookings Page; see [internal documentation page for this link](https://docs.ko-research.org/operating-procedures/meetings/)

    !!! tip "Things to Consider" 

        - Meeting request should contain suggested time, agenda, and objectives
        - Begin meeting with brief description of agenda and objectives (~5 minutes)
        - During meeting keep notes on a centralized note/slides repository
        - End meeting with brief documentation (~5 minutes)
        - [ ] optional recording

    !!! todo "Required entries"

        - meeting title
        - attendees
        - time and location (the PI office or Zoom)
        - agenda/objectives (key points to be covered)

??? tip "Early Birds Get the Worms"
    Schedule meetings as early as possible. Short notices may not always work.

??? note "Extended Readings"
    !!! tip "[The 11-step guide to running effective meetings](https://www.nature.com/articles/d41586-019-02295-z)"
        1. Do you need a meeting?
        2. Define the objective
        3. Set an agenda
        4. Keep it short
        5. Get the right people in the room
        6. Circulate materials
        7. Start on time, end on time
        8. Guide the discussion and manage disruptions
        9. Summarize decisions and next steps
        10. Circulate the meeting debrief
        11. Follow up on action items
