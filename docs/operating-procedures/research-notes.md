# Research Notes SOP

???+ example "SOP: Create a KRG Research Note Repository"
    - [Create a GitLab Group](https://docs.gitlab.com/ee/user/group/#create-a-group) with the project name (e.g., `rpff`)
    - [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) [The KRG research notes tempalte](https://gitlab.com/ko_research_group/research/research-notes) under the targeted project group namespace
    - [Unlink the fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork)
    - Place the research note repository link to [the project specific Teams channel](https://teams.microsoft.com/l/team/19%3a-A-EbmV8yIKIOEzuJ9nPIc3PxEQ-v3QOMm8qFYmzI-w1%40thread.tacv2/conversations?groupId=5835056d-9cc3-4168-b05b-5daec2330794&tenantId=70de1992-07c6-480f-a318-a1afcba03983)

???+ tip "visualization"
    - Automatically rendered page: https://`<group-name>`.gitlab.io/research-notes/
    - (optional) If not familiar with MKDocs and GitLab Pages, one can use Google Slides or PowerPoint on MS365 to assist presentation; however, complete (and verbose) research notes should be maintained in the associated git repository

???+ tip "record procedures and thoughts"
    - significance statement
    - literature
    - problem statement
    - proposed solution
    - software and procedure script
    - data (if not excessively large, say within 100 MB, use `git-lfs` for this purpose if possible)
    !!! tip "If in doubt, record everything and worry about cleaning up later"

???+ tip "keep it simple"
    Research notes provide the basis for more refined presentations such as conference talks and research papers.
    Since research notes are internal documents, keep it as simple as possible for fast turnover time and leave refinements when preparing manuscripts.

???+ tip "start/end each research meeting by reviewing/writing tasks"
    Use research notes as checkpoints to reduce overhead when restarting meetings.
