# Account Setup SOP

???+ abstract "GitLab"
    primary software DevOPs platform

    !!! note "steps"
        1. [Sign up for a free GitLab account](https://gitlab.com/users/sign_up)
        2. Send [Hsin-Yu](mailto:hsin-yu.ko@unt.edu) your `GitLab username` so that you can have access as a member of [the KRG Team](https://gitlab.com/ko_research_group/team)

???+ abstract "Zotero Group"
    access [KRG reference library](https://www.zotero.org/groups/5344838/krg/library)

    !!! note "steps"
        1. [Sign up for a free Zotero account](https://www.zotero.org/user/register)
        2. Send [Hsin-Yu](mailto:hsin-yu.ko@unt.edu) your `Zotero username` to be [invited](https://www.zotero.org/groups/5344838/settings/members) as a member
        3. Explore [group library](https://www.zotero.org/groups/5344838/krg/library)
        !!! warning "**INTERNAL USE ONLY**"
            By requesting access to KRG Zotero Group, you agree **not** to redistribute any of the documents. Exceptions must be requested to and granted by Hsin-Yu on a case-by-case basis.

???+ abstract "UNT"
    access university computers and software subscription

    !!! note "steps"
        1. After admission/enrollment, you will obtain your [UNT EUID](https://support.music.unt.edu/euid-and-password-management) and UNT email address.
        2. Request access to [KRG Teams Channel (using this link)](https://teams.microsoft.com/l/team/19%3a-A-EbmV8yIKIOEzuJ9nPIc3PxEQ-v3QOMm8qFYmzI-w1%40thread.tacv2/conversations?groupId=5835056d-9cc3-4168-b05b-5daec2330794&tenantId=70de1992-07c6-480f-a318-a1afcba03983)
        3. After joining the channel, please update contact information in [the KRG Admin Sheet](https://myunt.sharepoint.com/:x:/r/sites/KoResearchGroup/Shared%20Documents/admin.xlsx?d=w61a3c421b138404481aa8abb59b9d33c&csf=1&web=1&e=ErBj1h)
        4. After an account request for [CRUNTCH4](https://chemistry.unt.edu/research-facilities/computational-facilities) (made for you by Hsin-Yu), please follow instructions provided by Dr. Khetrapal to complete the setup.
