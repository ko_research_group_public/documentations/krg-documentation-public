# Question/Issue Handling SOP

!!! info "Workflow"
    1. Search KRG Docs. If problem not documented, go to next step.
    2. Web Search (e.g., Google and ChatGPT). If found solution, document briefly (e.g., solution url) to KRG Docs; otherwise, go to next step.
    3. Open an issue and ask a team member. If figured out a solution, document it to KRG Docs and close the issue; otherwise, one might found a potential research topic (discuss with Hsin-Yu about next steps).

    !!! info "When asking a team member"
        - give context (why do you care about the question?)
        - show effort (summarize what you have found in web search)
        - simplify unnecessary information
        - respect other people's time (request for an appointment on a convenient time)

!!! tip "Document all useful information"
    Similar questions can occur repeatedly. Elaborated documentation will typically benefit oneself the most.
