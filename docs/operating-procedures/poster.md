# Scientific Posters

???+ todo "template"

???+ info "printing"
    - [Eagle Images (Large Format Printing)](https://printingservices.unt.edu/eagle-images/index.html)
    - Size: A0 (33.1" x 46.8")
    - Paper: [matte (low reflection, ideal for high-lit areas like conference halls)](https://crescentprint.co.uk/reviewing-your-options-for-scientific-and-academic-poster-printing-in-edinburgh/)

???+ tip "timeline"
    - Typically need to finish (electronic) posters a week before conference to allow time for printing

???+ tip "further reading"
    - [ANL "Guide to Effective Poster Design"](https://www.anl.gov/education/guide-to-effective-poster-design)