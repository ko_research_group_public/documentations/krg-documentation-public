# Standard Operating Procedures

???+ abstract "Group Operation"

    - [Account Setup](account-setup.md)
    - [Meetings](meetings.md)
    - [Questions](questions.md)

???+ abstract "Research"

    - [Research Notes](research-notes.md)
    - [Time Management](time-management.md)

