# Welcome to KRG Documentation

???+ tip "Come work with us!"
    - [Prospective Student Resources](outreach/prospective-students/index.md)


## Recommended Use of This System

???+ example "General Search"
    Use the `search bar` in the top panel.

???+ example "Browse Core Documentations"
    Use the `high-level index` in the left panel.

## Frequently Used Documentation Pages

- [Getting Started (Lab Orientation)](training-programs/index.md)
- [Meeting Policy](operating-procedures/meetings.md)

## Other KRG Resources

- [Full KRG Documentation (Internal)](https://docs.ko-research.org/)
